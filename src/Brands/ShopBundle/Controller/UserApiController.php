<?php

namespace Brands\ShopBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('BrandsShopBundle:Default:index.html.twig', array('name' => $name));
    }
}
