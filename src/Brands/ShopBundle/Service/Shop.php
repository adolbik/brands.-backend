<?php

namespace Brands\ShopBundle\Service;

class Shop
{
	protected $container;

	public function __construct($container) {
		$this->container = $container;
	}

	/**
	 * Return entity with thumbnails path
	 * @var $entity
	 * @var string $fileContext FileLoader context
	 */
	public function addEntityLogoPath($entities, $fileContext) {
		$request = $this->container->get('request');
		$baseUrl = $request->getSchemeAndHttpHost();

		foreach($entities as $e) {
			if(!$e || !$e->getLogo()) {
				continue;
			}
			$logo = $e->getLogo();

			$fileLoader = $this->container->get('brands.file_loader');
			$fileLoader->configure($fileContext);
			$imageFolder = $fileLoader->getFolder();

			if($logo) {
				$imageOriginal = ($logo && isset($logo['original'])) ? $imageFolder.$logo['original'] : '';
				if($imageOriginal) {
					$cacheManager = $this->container->get('liip_imagine.cache.manager');
					$srcPath_54x50 = $cacheManager->getBrowserPath($imageOriginal, '54x50');
					$originalName = $logo['original'];

					$arr = array('original_name' => $originalName, '54x50' => $srcPath_54x50, 'path' => $baseUrl.$imageOriginal);
					$e->setLogo($arr);
				}
			}
		}

		return $entities;
	}
}
