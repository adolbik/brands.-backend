<?php

namespace Brands\ShopBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Doctrine\ORM\EntityRepository;
use Brands\CoreBundle\Helper\CatalogHelper;
use Brands\GoodsApiBundle\Entity\GoodsPhoto;

class ShopType extends AbstractType
{
	protected $oldLogo;
	protected $fileUploadHandler;

	public function __construct($fileUploadHandler = null) {
		$this->fileUploadHandler = $fileUploadHandler;
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$this->oldLogo = null;

        $builder
            ->add('name')
            ->add('address')
            ->add('workingTime','collection', array(
			   'type'   => 'text',
			   'options'  => array(
				   'required'  => false,
		   		),
			   'allow_add' => true,
			   'allow_delete' => true,
			))
			->add('externalCatalogLink', 'text', array(
					'required' => false
			))
        ;
        if($this->fileUploadHandler) {
            $builder
                ->add('logo', 'file', array('data_class' => null, 'required' => false))
                ->add('deleteLogo', 'integer', array( 'mapped' => false, 'required' => false));
            ;
            $builder->addEventListener(FormEvents::PRE_SUBMIT , array($this, 'saveOldLogo'));
            $builder->addEventListener(FormEvents::POST_SUBMIT , array($this, 'handleLogo'));
        }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Brands\ShopBundle\Entity\Shop',
			'csrf_protection'   => false
        ));
    }

	public function saveOldLogo(FormEvent $event) {
		$form = $event->getForm();
		$shop = $form->getData();

		$this->oldLogo = null; //$shop->getLogo();
	}

	public function handleLogo(FormEvent $event) {
		$form = $event->getForm();
		$shop = $form->getData();

		/* новый логотип */
		$logo = $form->get('logo')->getData();
		$fileLoader = $this->fileUploadHandler->fileLoader;
		$fileLoader->configure('temp');
		$tempFolder = $fileLoader->getFolder();

		$fileLoader->configure('shop_logo');

		if($form->isValid()) {
			// удаляем старое лого и грузим новое

			$logoArr = explode('/', $logo);
			$logo = array_pop($logoArr);

			if(!isset($this->oldLogo['original']) || $this->oldLogo['original'] != $logo) {
				if(isset($this->oldLogo['original']) && $this->oldLogo['original']) {
					$this->fileUploadHandler->removeFile($this->oldLogo['original'], 'shop_logo');
				}

				// сетим новую
				$oldPath = $fileLoader->getDocumentRoot().$tempFolder.$logo;

				if(file_exists($oldPath) && $logo) {
					/* перемещаем файл в постоянную папку */
					$fileLoader->moveFileInCurrentContext($oldPath, $logo);

					$shop->setLogo(array('original' => $logo));
				} else if(!$logo) {
					$shop->setLogo(array());
				}
			} else {
				$shop->setLogo(array('original' => $logo));
			}
		}
	}

    public function getName()
    {
        return 'shop';
    }
}
