<?php

namespace Brands\ShopBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use Brands\CoreBundle\Helper\CatalogHelper;

class RegisterShopType extends AbstractType
{
	public function __construct() {}

	public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array('required' => true))
            ->add('address')
            ->add('subscriptionType', 'entity', array(
                'class' => 'Brands\CoreBundle\Entity\CatalogItem',
                'query_builder' => function(EntityRepository $er){
                        return $er->createQueryBuilder("ci")
                            ->leftJoin('ci.catalog', 'catalog')
                            ->where('catalog.internalId = :catalogInternalId')
                            ->andWhere('ci.internalId <> :parserInternalId')
							->orderBy('ci.sort', 'ASC')
                            ->setParameters(array('catalogInternalId' => 'subscription_type', 'parserInternalId' => CatalogHelper::CATALOG_ITEM_SUBSCRIPTION_PARSER));
                    },
                "expanded" => true,
                "multiple" => false,
                'required' => true
            ))
            ->add('externalCatalogLink')
            ->add('phones', 'text', array('required' => true))
            ->add('site')
            /*->add('workingTime','collection', array(
			   'type'   => 'text',
			   'options'  => array(
				   'required'  => false,
		   		),
			   'allow_add' => true,
			   'allow_delete' => true,
			))*/
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Brands\ShopBundle\Entity\Shop',
			'csrf_protection'   => false
        ));
    }

    public function getName()
    {
        return 'shop';
    }
}
