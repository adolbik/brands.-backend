<?php

namespace Brands\ShopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="shop")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */

class Shop
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", nullable=false)
	 */
	protected $name;

	/**
	 * @var text
	 *
	 * @ORM\Column(name="description", type="text", nullable=true)
	 */
	protected $address;

	/**
	 * @var array
	 *
	 * @ORM\Column(name="working_time", type="array", nullable=true)
	 */
	protected $workingTime;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="site", type="string", nullable=true)
	 */
	protected $site;

    /**
	 * @var string
	 *
	 * @ORM\Column(name="external_catalog_link", type="string", nullable=true)
	 */
	protected $externalCatalogLink;

	/**
	 * @var array
	 *
	 * @ORM\Column(name="logo", type="array", nullable=true)
	 */
	protected $logo;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="phones", type="text", nullable=true)
	 */
	protected $phones;

	/**
	 * @var \DateTime $created_at
	 *
	 * @ORM\Column(name="created_at", type="datetime", nullable=FALSE)
	 */
	private $createdAt;

	/**
	 * @var \DateTime $updated_at
	 *
	 * @ORM\Column(name="updated_at", type="datetime", nullable=true)
	 */
	private $updatedAt;


	/**
	 * @ORM\ManyToOne(targetEntity="\Brands\UserBundle\Entity\User")
	 * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
	 */
	protected $owner;

    /**
	 * @ORM\ManyToOne(targetEntity="\Brands\CoreBundle\Entity\CatalogItem")
	 * @ORM\JoinColumn(name="subscription_type_id", referencedColumnName="id")
	 */
	protected $subscriptionType;


	/**
	 * @ORM\PrePersist
	 */
	public function prePersist()
	{
		$this->setCreatedAt(new \DateTime());
		$this->setUpdatedAt(new \DateTime());
	}

	/**
	 * @ORM\PreUpdate
	 */
	public function preUpdate()
	{
		$this->setUpdatedAt(new \DateTime());
	}

	public function isNew()
	{
		return $this->id ? false : true;
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Shop
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Shop
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set logo
     *
     * @param array $logo
     * @return Shop
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return array 
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Shop
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Shop
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set owner
     *
     * @param \Brands\UserBundle\Entity\User $owner
     * @return Shop
     */
    public function setOwner(\Brands\UserBundle\Entity\User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \Brands\UserBundle\Entity\User 
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set workingTime
     *
     * @param array $workingTime
     * @return Shop
     */
    public function setWorkingTime($workingTime)
    {
        $this->workingTime = $workingTime;

        return $this;
    }

    /**
     * Get workingTime
     *
     * @return array 
     */
    public function getWorkingTime()
    {
        return $this->workingTime;
    }

    /**
     * Set site
     *
     * @param string $site
     * @return Shop
     */
    public function setSite($site)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     *
     * @return string 
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * @param string $externalCatalogLink
     */
    public function setExternalCatalogLink($externalCatalogLink)
    {
        $this->externalCatalogLink = $externalCatalogLink;
    }

    /**
     * @return string
     */
    public function getExternalCatalogLink()
    {
        return $this->externalCatalogLink;
    }

    /**
     * Set subscriptionType
     *
     * @param \Brands\CoreBundle\Entity\CatalogItem $subscriptionType
     * @return Shop
     */
    public function setSubscriptionType(\Brands\CoreBundle\Entity\CatalogItem $subscriptionType = null)
    {
        $this->subscriptionType = $subscriptionType;

        return $this;
    }

    /**
     * Get subscriptionType
     *
     * @return \Brands\CoreBundle\Entity\CatalogItem 
     */
    public function getSubscriptionType()
    {
        return $this->subscriptionType;
    }

	/**
	 * @return string
	 */
	public function getPhones()
	{
		return $this->phones;
	}

	/**
	 * @param string $phones
	 */
	public function setPhones($phones)
	{
		$this->phones = $phones;
	}

}
