<?php

namespace Brands\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="category_to_user")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */

// Таблица создана для того, чтобы вести подсчет товаров продавца для каждой категории

class CategoryToUser
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	protected $id;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="count", type="integer", nullable=false, options={"default": 0})
	 */
	protected $count = 0;

	/**
	 * @ORM\ManyToOne(targetEntity="\Brands\UserBundle\Entity\User")
	 * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
	 */
	protected $user;

	/**
	 * @ORM\ManyToOne(targetEntity="Category")
	 * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
	 */
	protected $category;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set count
     *
     * @param integer $count
     * @return CategoryToUser
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Get count
     *
     * @return integer 
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Set user
     *
     * @param \Brands\UserBundle\Entity\User $user
     * @return CategoryToUser
     */
    public function setUser(\Brands\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Brands\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set category
     *
     * @param \Brands\AdminBundle\Entity\Category $category
     * @return CategoryToUser
     */
    public function setCategory(\Brands\AdminBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Brands\AdminBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }
}
