<?php

namespace Brands\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * KNPCategory
 *
 * @ORM\Entity
 * @ORM\Table(name="goods_category")
 * @ORM\Entity(repositoryClass="Brands\AdminBundle\Repository\CategoryRepository")
 * @ORM\HasLifecycleCallbacks()
 */

class Category implements ORMBehaviors\Tree\NodeInterface, \ArrayAccess
{
	use ORMBehaviors\Tree\Node
		//ORMBehaviors\Timestampable\Timestampable,
		//ORMBehaviors\SoftDeletable\SoftDeletable,
	    //ORMBehaviors\Blameable\Blameable
		//ORMBehaviors\Geocodable\Geocodable,
		//ORMBehaviors\Loggable\Loggable,
		//ORMBehaviors\Sluggable\Sluggable
		;

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", nullable=false)
	 */
	protected $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="internal_id", type="string", nullable=true)
	 */
	protected $internalId;

	/**
	 * @var $isForMale
	 *
	 * @ORM\Column(name="is_for_male", type="boolean", nullable=true, options={"default": 0})
	 */
	protected $isForMale;

	/**
	 * @var $isForFemale
	 *
	 * @ORM\Column(name="is_for_female", type="boolean", nullable=true, options={"default": 0})
	 */
	protected $isForFemale;

	/**
	 * @var $synonyms
	 *
	 * @ORM\Column(name="synonyms", type="text", nullable=true)
	 */
	protected $synonyms;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="count_goods", type="integer", nullable=false, options={"default": 0})
	 */
	protected $countGoods = 0;


	/**
	 * @var \Doctrine\Common\Collections\Collection
	 *
	 * @ORM\ManyToMany(targetEntity="\Brands\CoreBundle\Entity\CatalogItem")
	 * @ORM\JoinTable(name="category_to_size",
	 *   joinColumns={
	 *     @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="CASCADE")
	 *   },
	 *   inverseJoinColumns={
	 *     @ORM\JoinColumn(name="size_id", referencedColumnName="id")
	 *   }
	 * )
	 */
	protected $sizes;

	/**
	 * @ORM\OneToMany(targetEntity="\Brands\GoodsApiBundle\Entity\Goods", mappedBy="category", orphanRemoval=true, cascade={"persist", "remove"})
	 */
	protected $goods;

	/**
	 * @ORM\OneToMany(targetEntity="CategoryToUser", mappedBy="category")
	 */
	protected $categoryToUser;


	/**
	 * В этом переменной храним parent, когда запись приходит с формы
	 */
	protected $parent;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->sizes = new \Doctrine\Common\Collections\ArrayCollection();
		$this->goods = new \Doctrine\Common\Collections\ArrayCollection();
	}


	public function getId() {
		return $this->id;
	}

	public function getSluggableFields()
	{
		return array('internalId');
	}

	/**
	 * Returns whether or not the slug gets regenerated on update.
	 *
	 * @return bool
	 */
	private function getRegenerateSlugOnUpdate()
	{
		return false;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 * @return Category
	 */
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Set internalId
	 *
	 * @param string $internalId
	 * @return Category
	 */
	public function setInternalId($internalId)
	{
		$this->internalId = $internalId;

		return $this;
	}

	/**
	 * Get internalId
	 *
	 * @return string
	 */
	public function getInternalId()
	{
		return $this->internalId;
	}

	/**
	 * @param mixed $parent
	 */
	public function setParent($parent)
	{
		$this->parent = $parent;
	}

	/**
	 * @return mixed
	 */
	public function getParent()
	{
		return $this->parent;
	}

	/**
	 * @return mixed
	 */
	public function getParentId()
	{
		$arr = $this->getParentsIds();

		return array_pop($arr);
	}

	/**
	 * @return mixed
	 */
	public function getParentsIds()
	{
		$arr = explode($this->getMaterializedPathSeparator(), $this->materializedPath);

		return $arr;
	}


	/**
	 * Add sizes
	 *
	 * @param \Brands\CoreBundle\Entity\CatalogItem $sizes
	 * @return Category
	 */
	public function addSize(\Brands\CoreBundle\Entity\CatalogItem $sizes)
	{
		$this->sizes[] = $sizes;

		return $this;
	}

	/**
	 * Remove sizes
	 *
	 * @param \Brands\CoreBundle\Entity\CatalogItem $sizes
	 */
	public function removeSize(\Brands\CoreBundle\Entity\CatalogItem $sizes)
	{
		$this->sizes->removeElement($sizes);
	}

	/**
	 * Get sizes
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getSizes()
	{
		return $this->sizes;
	}

	/**
	 * @param int $countGoods
	 */
	public function setCountGoods($countGoods)
	{
		$this->countGoods = $countGoods;
	}

	/**
	 * @return int
	 */
	public function getCountGoods()
	{
		return $this->countGoods;
	}

	/**
	 * Add goods
	 *
	 * @param \Brands\GoodsApiBundle\Entity\Goods $goods
	 * @return Caregory
	 */
	public function addGoods(\Brands\GoodsApiBundle\Entity\Goods $goods)
	{
		$this->goods[] = $goods;

		return $this;
	}

	/**
	 * Set goods
	 *
	 * @return Caregory
	 */
	public function setGoods($goods)
	{
		$this->goods = $goods;

		return $this;
	}

	/**
	 * Remove goods
	 *
	 * @param \Brands\GoodsApiBundle\Entity\Goods $goods
	 */
	public function removePhoto(\Brands\GoodsApiBundle\Entity\Goods $goods)
	{
		$this->goods->removeElement($goods);
	}

	/**
	 * Get goods
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getGoods()
	{
		return $this->goods;
	}

	/**
	 * Set isForMale
	 *
	 * @param integer $isForMale
	 * @return Goods
	 */
	public function setIsForMale($isForMale)
	{
		$this->isForMale = $isForMale;

		return $this;
	}

	/**
	 * Get isForMale
	 *
	 * @return boolean
	 */
	public function getIsForMale()
	{
		return $this->isForMale;
	}

	/**
	 * Set isForFemale
	 *
	 * @param integer $isForFemale
	 * @return Goods
	 */
	public function setIsForFemale($isForFemale)
	{
		$this->isForFemale = $isForFemale;

		return $this;
	}

	/**
	 * Get isForFemale
	 *
	 * @return boolean
	 */
	public function getIsForFemale()
	{
		return $this->isForFemale;
	}

	/**
	 * @return mixed
	 */
	public function getSynonyms()
	{
		return $this->synonyms;
	}

	/**
	 * @param mixed $synonyms
	 */
	public function setSynonyms($synonyms)
	{
		$this->synonyms = $synonyms;
	}

}
