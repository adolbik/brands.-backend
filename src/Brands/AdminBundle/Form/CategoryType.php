<?php

namespace Brands\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Doctrine\ORM\EntityRepository;
use Brands\CoreBundle\Helper\CatalogHelper;

class CategoryType extends AbstractType
{
	protected $_categoryService;
	protected $_catalogService;

	public function __construct($categoryService, $catalogService) {
		$this->_categoryService = $categoryService;
		$this->_catalogService = $catalogService;
	}

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('internalId')
			->add('parent', 'entity', array(
				 'class'     => 'Brands\AdminBundle\Entity\Category',
				 'expanded'	=> false,
				 'required'  => false,
				 'multiple' => false,
				 //'mapped' => false,
				 'property' => 'name'
			))
			->add('sizes', 'entity', array(
			  	'class' => 'BrandsCoreBundle:CatalogItem',
			  	/*'query_builder' => function(EntityRepository $er) {
					return $er->createQueryBuilder('i')
						->leftJoin('i.catalog', 'catalog')
						->andWhere('catalog.internalId = :cataloginternalId')
						->orderBy('i.sort', 'ASC')
						->setParameter('cataloginternalId', CatalogHelper::CATALOG_SIZES)
						;
				},*/
				'choices' => $this->_catalogService->getAllItems(CatalogHelper::CATALOG_SIZES),
				'expanded'	=> true,
				'required'  => false,
				'multiple' => true
		 	));
        ;

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Brands\AdminBundle\Entity\Category'
        ));
    }

	/*protected function getSizesChoices($entity) {
		$category = $entity->getParent();

		$sizes = array();
		if(!count($sizes = $category->getSizes())) {
			$sizes = $this->_categoryService->getParentCategoriesSizes($category->getId());
			if(!$sizes) {
				$sizes = $this->_catalogService->getAllItems(CatalogHelper::CATALOG_SIZES);
			}
		}

		return $sizes;
	}*/

    public function getName()
    {
        return 'brands_adminbundle_category';
    }
}
