<?php

namespace Brands\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Doctrine\ORM\EntityRepository;
use Brands\CoreBundle\Helper\CatalogHelper;

class GoodsModerationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('isModerated')
            ->add('isRejected')
            ->add('rejectComment')
        ;

        $builder->addEventListener(FormEvents::POST_SUBMIT , array($this, 'checkModeratedFlag'));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Brands\GoodsApiBundle\Entity\Goods'
        ));
    }

    public function checkModeratedFlag(FormEvent $event) {
        $form = $event->getForm();
        $goods = $form->getData();

        if($goods->getIsModerated()) { // если отмечена модерация, то снимаем флаг isRejected, даже если это забылисделать в админке
            $goods->setIsRejected(0);
        }
    }

    public function getName()
    {
        return 'brands_adminbundle_goods_moderation';
    }
}
