<?php
namespace Brands\AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Knp\DoctrineBehaviors\ORM as ORMBehaviors;

class CategoryRepository extends EntityRepository
{
	use ORMBehaviors\Tree\Tree;

    /**
     * Returns a node hydrated with its children and parents
     *
     * @api
     *
     * @param string $path
     * @param string $rootAlias
     *
     * @return NodeInterface a node
     */
    public function getTree($path = '', $withNesting = true, $rootAlias = 't')
    {
        $results = $this->getFlatTree($path, $rootAlias);

		return $this->buildTree($results, $withNesting);
    }

    /**
     * Extracts the root node and constructs a tree using flat resultset
     *
     * @param Iterable|array $results a flat resultset
     *
     * @return NodeInterface
     */
    public function buildTree($results, $withNesting = true)
    {
        if (!count($results)) {
            return;
        }

        $response = array();
        foreach($results as $k => $root) {
            if($withNesting && (!$root->getMaterializedPath() || $k == 0)) {
                $root->buildTree($results);
                $response[] = $root;
            } else if (!$withNesting) {
                //$root->buildTree($results);
                $response[] = $root;
            }
        }

        return $response;
    }

	public function getTreeExceptNodeAndItsChildren($entity) {
		return $this
			->getTreeExceptNodeAndItsChildrenQB($entity)
			->getQuery()
			->execute()
			;
	}

	public function getFirstParentQB(NodeInterface $entity, $rootAlias = 't')
	{
		return $this->getFlatTreeQB('', $rootAlias)
			->andWhere($rootAlias.'.materializedPath NOT LIKE :except_path')
			->andWhere($rootAlias.'.id != :id')
			->andWhere($rootAlias.'.id LIKE :except_path')
			->setParameter('except_path', $entity->getRealMaterializedPath().'%')
			->setParameter('child_path', $entity->getRealMaterializedPath().'%')
			->setParameter('id', $entity->getId())
			;
	}

	public function findWithRelated($categoryId)
	{
		$qb =  $this->createQueryBuilder('c');

		$qb ->select('c', 's')
			->leftJoin('c.sizes', 's')
			->where('c.id = :id')
			->setParameter(':id', $categoryId)
		;

		try {
			return $qb->getQuery()->getSingleResult();
		} catch (\Doctrine\ORM\NoResultException $e) {
			return null;
		}
	}

	public function getParentsCategories($childCategoryId, $withRelated=false)
	{
		$qb =  $this->createQueryBuilder('c');

		$qb ->select('c1')
			->add('from', 'BrandsAdminBundle:Category c2, BrandsAdminBundle:Category c1')
			->where("c2.materializedPath LIKE CONCAT('%/', c1.id, '%')")
			->andWhere('c2.id = :categoryId')
			->orderBy('c2.materializedPath', 'DESC')
			->setParameter(':categoryId', $childCategoryId)
		;

		if($withRelated) {
			$qb->addSelect('s');
			$qb->leftJoin('c1.sizes', 's');
		}

		try {
			return $qb->getQuery()->getResult();
		} catch (\Doctrine\ORM\NoResultException $e) {
			return null;
		}
	}

	public function findCategoryBySynonyms($name)
	{
		$qb =  $this->createQueryBuilder('c');

		$qb ->select('c')
			->where('c.synonyms LIKE :likeName OR c.name = :name')
			->orderBy('c.materializedPath', 'DESC')
		;

		$qb->setParameters(array('likeName' => ('%,'.$name.',%'), 'name' => $name));

		try {
			return $qb->getQuery()->getOneOrNullResult();
		} catch (\Doctrine\ORM\NoResultException $e) {
			return null;
		}
	}

}