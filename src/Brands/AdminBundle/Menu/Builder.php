<?php

namespace Brands\AdminBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware
{
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root')->setChildrenAttributes(array('class'=>'nav nav-list bs-docs-sidenav sidebar-nav visible-desktop'));

        $menu->addChild('Home', array('route' => 'category'));
        $menu->addChild('Категории', array('route' => 'category'));
        $menu->addChild('Товары', array('route' => 'goods'));

        return $menu;
    }

    public function topMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root')->setChildrenAttributes(array('class'=>'nav'));

        $menu->addChild('Home', array('route' => ''));
        $menu->addChild('Getting started?', array('route' => ''));

        return $menu;
    }

}