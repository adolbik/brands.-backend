<?php

namespace Brands\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Brands\AdminBundle\Entity\Category;
use Brands\AdminBundle\Form\CategoryType;

use Imagine\Exception\Exception;

/**
 * Category controller.
 *
 */
class CategoryController extends Controller
{

    /**
     * Lists all Category entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BrandsAdminBundle:Category')->findAll();

        return $this->render('BrandsAdminBundle:Category:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Category entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Category();
        $form = $this->createForm(new CategoryType($this->get('brands.category'), $this->get('brands.catalog')), $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

			if($entity->getParent()) {
				$this->get('brands.category')->addChildToParent($entity);
			}

            $this->get('session')->getFlashBag()->add('notice', 'The entry is successfuly added!');

            return $this->redirect($this->generateUrl('category_show', array('id' => $entity->getId())));
        }

        return $this->render('BrandsAdminBundle:Category:edit.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Category entity.
     *
     */
    public function newAction()
    {
        $entity = new Category();
        $form   = $this->createForm(new CategoryType($this->get('brands.category'), $this->get('brands.catalog')), $entity);

        return $this->render('BrandsAdminBundle:Category:edit.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Category entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BrandsAdminBundle:Category')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Category entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BrandsAdminBundle:Category:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Category entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BrandsAdminBundle:Category')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Category entity.');
        }

		$parent = $em->getRepository('BrandsAdminBundle:Category')->find($entity->getParentId());
		$entity->setParent($parent);

        $editForm = $this->createForm(new CategoryType($this->get('brands.category'), $this->get('brands.catalog')), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BrandsAdminBundle:Category:edit.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
	 * Edits an existing Category entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BrandsAdminBundle:Category')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Category entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new CategoryType($this->get('brands.category'), $this->get('brands.catalog')), $entity);
        $editForm->submit($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

			if($entity->getParent()) {
				$this->get('brands.category')->addChildToParent($entity);
			}

			$this->get('session')->getFlashBag()->add('notice', 'Your changes were saved!');

            return $this->redirect($this->generateUrl('category_edit', array('id' => $id)));
        }

        return $this->render('BrandsAdminBundle:Category:edit.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Category entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $categoriesWithChild = $em->getRepository('BrandsAdminBundle:Category')->getTree('%/'.$id);

        foreach($categoriesWithChild as $category) {
            $this->recursiveRemoveCategoryWithGoods($category);
        }
        $em->flush();

        return $this->redirect($this->generateUrl('category'));
    }

    protected function recursiveRemoveCategoryWithGoods($category) {
        $em = $this->getDoctrine()->getManager();
        $goodsService = $this->get('brands.goods');

        // удаляем детей, а потом и саму категорию
        foreach($category->getChildNodes() as $categoryChild) {
            $this->recursiveRemoveCategoryWithGoods($categoryChild);
        }

        $categoryGoods = $category->getGoods();
        foreach($categoryGoods as $g) {
            $goodsService->removeGoods($g);
        }
        $em->remove($category);

        return true;
    }

    /**
     * Creates a form to delete a Category entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
