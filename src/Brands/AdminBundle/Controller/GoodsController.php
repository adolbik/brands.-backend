<?php

namespace Brands\AdminBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Brands\AdminBundle\Form\GoodsModerationType;
use Brands\GoodsApiBundle\Form\GoodsType;

/**
 * Category controller.
 *
 */
class GoodsController extends Controller
{

    /**
     * Lists all Goods entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BrandsGoodsApiBundle:Goods')->findAllWithRelated();

        return $this->render('BrandsAdminBundle:Goods:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Lists Goods entities on moderation.
     *
     */
    public function moderationGoodsAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BrandsGoodsApiBundle:Goods')->findNotModeratedGoods();

        return $this->render('BrandsAdminBundle:Goods:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a Category entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BrandsGoodsApiBundle:Goods')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Goods entity.');
        }

        $moderationForm = $this->createForm(new GoodsModerationType(), $entity);

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BrandsAdminBundle:Goods:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'moderation_form' => $moderationForm->createView()
        ));
    }

    /**
     * Save moderation form.
     *
     */
    public function saveModerationFormAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BrandsGoodsApiBundle:Goods')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Category entity.');
        }

        $editForm = $this->createForm(new GoodsModerationType(), $entity);
        $editForm->submit($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'Your changes were saved!');

            return $this->redirect($this->generateUrl('goods_show', array('id' => $id)));
        }
    }

    /**
     * Displays a form to edit an existing Category entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BrandsGoodsApiBundle:Goods')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Goods entity.');
        }

		$entity->setImages($entity->getPhotos());

        $editForm = $this->createForm(new GoodsType($em, $this->get('brands.file_upload_handler'), $this->get('brands.category')), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BrandsAdminBundle:Goods:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
	 * Edits an existing Category entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BrandsGoodsApiBundle:Goods')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Goods entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new GoodsType($em, $this->get('brands.file_upload_handler'), $this->get('brands.category')), $entity);
        $editForm->submit($request);

        if ($editForm->isValid()) {
            //	$em->persist($entity);
            $em->flush();

			$this->get('session')->getFlashBag()->add('notice', 'Your changes were saved!');

            return $this->redirect($this->generateUrl('goods_edit', array('id' => $id)));
        }

        return $this->render('BrandsAdminBundle:Goods:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Category entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BrandsGoodsApiBundle:Goods')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Goods entity.');
        }

        $this->get('brands.goods')->removeGoods($entity);

        $this->get('session')->getFlashBag()->add('notice', 'The entry is deleted!');

        return $this->redirect($this->generateUrl('goods'));
    }

    /**
     * Creates a form to delete a Category entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
