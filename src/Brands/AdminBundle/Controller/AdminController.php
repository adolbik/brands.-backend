<?php

namespace Brands\AdminBundle\Controller;

use Brands\AdminBundle\Entity\Category;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Admin controller.
 *
 */
class AdminController extends Controller
{

    /**
     * Index page
     *
     */
    public function indexAction()
    {

    }

	/**
	 * Добавляем кастомные категории
	 *
	 */
	public function addCustomAction()
	{
		$em = $this->getDoctrine()->getManager();

// men
$categories = array(
	array(
		'parent_id' => 1,
		'zenskoe' => array(
			'Блузки, рубашки',
			'Болеро',
			'Для офиса',
			'Брюки',
			'Верхняя одежда',
			'Водолазки',
			'Джинсы',
			'Комбинезоны',
			'Костюмы',
			'Кофточки',
			'Кофты, кардиганы, твинсеты',
			'Лонгсливы',
			'Одежда для дома',
			'Пиджаки, жакеты',
			'Платья',
			'Свитеры, пуловеры, джемперы',
			'Свитшоты, толстовки',
			'Топы',
			'Туники',
			'Футболки',
			'Шорты, бермуды',
			'Юбки',
		),
		'myzskoe' => array(
			'Брюки',
			'Верхняя одежда',
			'Джинсы',
			'Жилеты',
			'Костюмы',
			'Кофты, кардиганы, твинсеты',
			'Лонгсливы',
			'Одежда для дома',
			'Пиджаки',
			'Рубашки',
			'Свитеры, пуловеры, джемперы',
			'Свитшоты, толстовки',
			'Футболки и поло',
			'Шорты, бермуды'
		)
	),
	array(
		'parent_id' => 2,
		'zenskoe' => array(
			'Балетки',
			'Босоножки, сандалии',
			'Ботильоны',
			'Ботинки',
			'Валенки',
			'Дутики',
			'Кеды',
			'Кроссовки',
			'Мокасины',
			'Пантолеты, шлепанцы',
			'Полусапожки',
			'Резиновые сапоги',
			'Сабо',
			'Сапоги',
			'Тапочки',
			'Туфли',
			'Угги',
			'Унты',
		),
		'myzskoe' => array(
			'Ботинки',
			'Бутсы',
			'Кеды',
			'Кроссовки',
			'Мокасины',
			'Пантолеты, шлепанцы',
			'Сандалии',
			'Сапоги',
			'Тапочки',
			'Туфли',
		)
	),
	array(
		'parent_id' => 3,
		'zenskoe' => array(
			'Аксессуары для волос',
			'Бижутерия',
			'Зеркальца'
		),
		'myzskoe' => array(
			'Браслеты',
			'Галстуки',
			'Зажигалки',
			'Запонки'
		),
		'all' => array(
			'Для планшета или электронной книги',
			'Для телефона',
			'Кошельки, обложки для документов, визитницы',
			'Очки',
			'Перчатки',
			'Сумки и рюкзаки',
			'Ремни, пояса',
			'Часы',
			'Шарфы, платки',
			'Зонты',
			'Головные уборы'
		)
	)
);

		foreach($categories as $item) {
			$parent = $em->getRepository('BrandsAdminBundle:Category')->find($item['parent_id']);
			foreach($item['zenskoe'] as $name) {
				$category = new Category();
				$category->setName($name);
				$category->setInternalId('zhenskie_'.$this->getInTranslit($name));
				$category->setIsForMale(0);
				$category->setIsForFemale(1);

				$em->persist($category);
				$em->flush();

				$category->setParent($parent);
				$this->get('brands.category')->addChildToParent($category);
			}
			foreach($item['myzskoe'] as $name) {
				$category = new Category();
				$category->setName($name);
				$category->setInternalId('muzhskie_'.$this->getInTranslit($name));
				$category->setIsForMale(1);
				$category->setIsForFemale(0);

				$em->persist($category);
				$em->flush();

				$category->setParent($parent);
				$this->get('brands.category')->addChildToParent($category);
			}
			if(isset($item['all'])) {
				foreach ($item['all'] as $name) {
					$category = new Category();
					$category->setName($name);
					$category->setInternalId('' . $this->getInTranslit($name));
					$category->setIsForMale(1);
					$category->setIsForFemale(1);

					$em->persist($category);
					$em->flush();

					$category->setParent($parent);
					$this->get('brands.category')->addChildToParent($category);
				}
			}


		}

		return new Response('Completed!');
	}

	protected function getInTranslit($string) {
		$replace=array(
			"'"=>"",
			"`"=>"",
			" " => "",
			"," => "_",
			"а"=>"a","А"=>"a",
			"б"=>"b","Б"=>"b",
			"в"=>"v","В"=>"v",
			"г"=>"g","Г"=>"g",
			"д"=>"d","Д"=>"d",
			"е"=>"e","Е"=>"e",
			"ж"=>"zh","Ж"=>"zh",
			"з"=>"z","З"=>"z",
			"и"=>"i","И"=>"i",
			"й"=>"y","Й"=>"y",
			"к"=>"k","К"=>"k",
			"л"=>"l","Л"=>"l",
			"м"=>"m","М"=>"m",
			"н"=>"n","Н"=>"n",
			"о"=>"o","О"=>"o",
			"п"=>"p","П"=>"p",
			"р"=>"r","Р"=>"r",
			"с"=>"s","С"=>"s",
			"т"=>"t","Т"=>"t",
			"у"=>"u","У"=>"u",
			"ф"=>"f","Ф"=>"f",
			"х"=>"h","Х"=>"h",
			"ц"=>"c","Ц"=>"c",
			"ч"=>"ch","Ч"=>"ch",
			"ш"=>"sh","Ш"=>"sh",
			"щ"=>"sch","Щ"=>"sch",
			"ъ"=>"","Ъ"=>"",
			"ы"=>"y","Ы"=>"y",
			"ь"=>"","Ь"=>"",
			"э"=>"e","Э"=>"e",
			"ю"=>"yu","Ю"=>"yu",
			"я"=>"ya","Я"=>"ya",
			"і"=>"i","І"=>"i",
			"ї"=>"yi","Ї"=>"yi",
			"є"=>"e","Є"=>"e"
		);
		return $str=iconv("UTF-8","UTF-8//IGNORE",strtr($string,$replace));
	}
}
