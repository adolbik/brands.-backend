<?php

namespace Brands\AdminBundle\Service;

use Symfony\Component\HttpFoundation\Session\Session;
use Brands\AdminBundle\Entity\CategoryToUser;
use Doctrine\ORM\EntityManager;

class Category
{
	private $_em;

	/**
	 * @param $em
	 */
	public function __construct(EntityManager $em)
	{
		$this->_em = $em;
	}

	public function addChildToParent(\Brands\AdminBundle\Entity\Category $child) {
		$parent = $child->getParent();
		$childTree = $this->_em->getRepository('BrandsAdminBundle:Category')->getTree('%/'.$child->getId());
        $childTree = $childTree ? $childTree[0] : null;

		if($childTree) {
			$childTree->setChildNodeOf($parent);
			$this->_em->flush();
		}
	}

	public function getCategorySizes(int $id) {
		$category = $this->_em->getRepository('BrandsAdminBundle:Category')->findWithSizes($id);

		return $category->getSizes();
	}

	/**
	 * Находит размерную сетку у ближайшего родителя
	 * @param $chieldCategoryId
	 */
	public function getParentCategoriesSizes($childCategoryId) {
		$parentCategories = $this->_em->getRepository('BrandsAdminBundle:Category')->getParentsCategories($childCategoryId, true);

		$sizes = array();
		foreach($parentCategories as $pCategory) {
			if($pCategory->getSizes()) {
				$sizes = $pCategory->getSizes();
				break;
			}
		}

		return $sizes;
	}

	/**
	 * Считаем кол-во товаров для категории / для категории с учетом принадлежности товара пользователю
	 * @param      $categoryId
	 * @param null $userId
	 *
	 * @return mixed
	 */
	public function calculateCategoryGoods($categoryId, $userId = null) {
		$count = $this->_em->getRepository('BrandsGoodsApiBundle:Goods')->getCountCategoryGoods($categoryId, $userId);

		return $count;
	}

	/**
	 * Устанавливаем кол-во товаров для связки юзер-категория
	 * @param $category
	 * @param $user
	 * @param bool $considerNewGoods Учитывать ли тот факт, что новый товар еще не сохранен
	 *
	 * @return mixed
	 */
	public function setCategoryUserGoodsCount($category, $user, $considerNewGoods = false) {
		$categoryId =  $category->getId();
		$count = $this->_em->getRepository('BrandsGoodsApiBundle:Goods')->getCountCategoryGoods($category->getId(), $user->getId());
		$categoryToUser = $this->_em->getRepository('BrandsAdminBundle:CategoryToUser')->findOneBy(array('user' => $user->getId(), 'category' => $categoryId));
		if(!$categoryToUser) {
			$categoryToUser = new CategoryToUser();
			$categoryToUser->setCategory($category);
			$categoryToUser->setUser($user);

			$this->_em->persist($categoryToUser);
		}

		$count = $considerNewGoods ? ($count+1) : $count;
		$categoryToUser->setCount($count);

		// проходимся по родителям категории и пересчитываем для них
		$parentsCategories = $this->_em->getRepository('BrandsAdminBundle:Category')->getParentsCategories($categoryId);
		$firstParentCategory = count($parentsCategories) ? $parentsCategories[0] : null;
		if($firstParentCategory) {
			$this->setCategoryUserGoodsCount($firstParentCategory, $user, $considerNewGoods);
		}

		return $count;
	}

	/**
	 * @param      $category
	 * @param bool $considerNewGoods Учитывать ли тот факт, что новый товар еще не сохранен
	 *
	 * @return mixed
	 */
	public function setCategoryGoodsCount($category, $considerNewGoods = false) {
		$categoryId = $category->getId();
		$count = $this->_em->getRepository('BrandsGoodsApiBundle:Goods')->getCountCategoryGoods($categoryId);
		$count = $considerNewGoods ? ($count+1) : $count;

		$category->setCountGoods($count);

		// проходимся по родителям категории и пересчитываем для них
		$parentsCategories = $this->_em->getRepository('BrandsAdminBundle:Category')->getParentsCategories($categoryId);
		$firstParentCategory = count($parentsCategories) ? $parentsCategories[0] : null;
		if($firstParentCategory) {
			$this->setCategoryGoodsCount($firstParentCategory, $considerNewGoods);
		}

		return $count;
	}

}