<?php

namespace Brands\CoreBundle\Manager\File;

use Imagine\Imagick\Imagine;
use Imagine\Image\ImageInterface;
use Imagine\Image\Box;
use Imagine\Image\Point;

/**
 * FileManager
 */
class FileManager
{
	/**
	 * Check exist directory. Create if not exist.
	 * 
	 * @param  string $directory
	 */
	public function checkDirectory( $directory )
	{
		if (!is_dir($directory)) {
			$oldmask = umask(0);
			mkdir($directory, 0777, true);
			umask($oldmask);
		}
	}

	/**
	 * Generate unique file name
	 * @param  string $uploadDir 
	 * @param  string $fileExtension      
	 * @return string
	 */
	public function generateFilename( $uploadDir, $fileExtension )
	{
		$length = 8;
		$characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    
	    do {
		   	$filename = '';
		    for ($i = 0; $i < $length; $i++) {
		        $filename .= $characters[rand(0, strlen($characters) - 1)];
		    }
		} while (file_exists($uploadDir . '/' . $filename . '.' . $fileExtension));

		return $filename;
	}

	/**
     * Build thumbnails for image file and save to disc.
     * 
     * @param  string $file
     * @param  string $filename generated file name
     * @param  string $documentRoot
     * @param  array  $config
     */
    public function buildThumbnails( $file, $filename, $documentRoot, $config )
    {
        // If config not contain need sizes - thumbnails not needed
        if ( !isset($config['imageSizes']) || empty($config['imageSizes'])) {
            return;
        }

        // Get sizes
        $sizes = $config['imageSizes'];

        $imagine = new Imagine();

        foreach ($sizes as $key => $value) {
            // Build save directory
            $uploadDir = $documentRoot . $config['directory'] ;//. '/' . $key;
            // Check exist directory, and creation if not exist
            $this->checkDirectory($uploadDir);

            // Fill attributes
            $width   = $value['width'];
            $height  = $value['height'];
            $mode    = isset($value['mode']) ? $value['mode'] : null;


            // Load image
            $image = $imagine->load($file);
            
            // Resize
            if (null == $width) {
                $image = $image->resize($image->getSize()->heighten($height));
            } elseif (null == $height) {
                $image = $image->resize($image->getSize()->widen($width));
            } else {
                switch($mode) {
                    case 'outbound':
                        $mode = ImageInterface::THUMBNAIL_OUTBOUND;
                        break;
                    case 'inset':
                        $mode = ImageInterface::THUMBNAIL_INSET;
                        break;
					case 'top_aria':
                        $mode = ImageInterface::THUMBNAIL_TOP;
                        break;
                    default:
                        $mode = ImageInterface::THUMBNAIL_OUTBOUND;
                }

                //$image = $image->thumbnail(new Box($width, $height), $mode);

                $size = new Box($width, $height);
                $real_size = $image->getSize();
                $ratios = array(
                    $width / $real_size->getWidth(),
                    $height / $real_size->getHeight()
                );

                $thumbnail = $image->copy();

                if ($mode === ImageInterface::THUMBNAIL_INSET) {
                    $ratio = min($ratios);
                } else {
                    $ratio = max($ratios);
                }

                $thumbnailSize = $thumbnail->getSize()->scale($ratio);
                $thumbnail->resize($thumbnailSize);

                if ($mode === ImageInterface::THUMBNAIL_OUTBOUND) {
                    $thumbnail->crop(new Point(
                        max(0, round(($thumbnailSize->getWidth() - $width) / 2)),
                        max(0, round(($thumbnailSize->getHeight() - $height) / 2))
                    ), $size);
                } else if ($mode === ImageInterface::THUMBNAIL_TOP) {
                    $thumbnail->crop(new Point(
                        max(0, round(($thumbnailSize->getWidth() - $width) / 2)),
                        0
                    ), $size);
                }

                $image = $thumbnail;
            }

            $filename_arr = explode('.', $filename);
            $filename_ext = array_pop($filename_arr);
            $new_filename = implode('.', $filename_arr);
          

            // Save to disc
            $image->save($uploadDir . '/' . $new_filename . '_' . $key . '.' . $filename_ext);
        }
    }
}