<?php

namespace Brands\CoreBundle\Manager\File;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraints\File as FileConstraint;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * FileLoader
 */
class FileLoader
{
	/**
     * @var ContainerInterface $container
     */
    private $container;

    /**
     * Context config
     * 
     * @var array
     */
    private $config = array();

    /**
     * Document root directory
     * 
     * @var string
     */
    private $documentRoot;

    /**
     * Constructs a new instance of CookieManager.
     *
     * @param ContainerInterface $container The container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Configure service
     * @param  string $context 
     */
    public function configure($context, $config = [])
    {
		$container = $this->container;

		if(!$config) {
			// get data from config file
			$param = $container->getParameter('brands_uploads');
			$defaultConfig = $param['default'];
			$contextConfig = $param['context'];

			// merge default option with context options
			$this->config = array_merge($defaultConfig, $contextConfig[$context]);
		} else {
			$this->config = $config;
		}

    	// detect document root directory
    	$this->documentRoot = $container->get('request')->server->get('DOCUMENT_ROOT');
    }

	/**
	 * Validate file by conditions from config file
	 * @param  UploadedFile $file
	 * @return \Symfony\Component\Validator\ConstraintViolationList
	 */
	public function validate( UploadedFile $file )
	{
		$validator = $this->container->get('validator');
		$fileConstraint = new FileConstraint(array(
			  'maxSize' => $this->config['maxSize'],
			  'mimeTypes' => $this->config['mimeTypes'],
		 ));
		$errors = $validator->validateValue($file, $fileConstraint);
		return $errors;
	}

	/**
	 * Save file to disc and to db
	 * @param  UploadedFile $file
	 * @param  boolean $withPersist
	 * @return Media
	 */
	public function save($file, $filename = '', $withPersist = false)
	{
		$fileManager = $this->container->get('brands.file_manager');
		// Build save directory
		$uploadDir = $this->documentRoot . $this->config['directory'];
		// Check exist directory, and creation if not exist
		$fileManager->checkDirectory($uploadDir);

		if(!$filename) {
			$filename = $fileManager->generateFilename( $uploadDir, $file->guessExtension() );
		}
		$fullFilename = $filename . '.' . $file->guessExtension();
		//$fullFilename = $filename;

		// If file - image, create and save thumbnails
		if (preg_match("/image\/(.*)/", $file->getClientMimeType()) > 0) {
			$fileManager->buildThumbnails(
				file_get_contents($file->getPathname()),
				$fullFilename,
				$this->documentRoot,
				$this->config
			);
		}

		// Save file
		$file->move($uploadDir, $fullFilename);

		// Build entity
		/*$media = new Media();
		$media
			->setTitle( $file->getClientOriginalName() )
			->setMimeType( $file->getClientMimeType() )
			->setFilename( $fullFilename )
			->setSize( $file->getClientSize() )
			->setIsEmbed( false )
		;

		// Save entity
		if ( $withPersist ) {
			$em = $this->container->get('doctrine')->getEntityManager();
			$em->persist($media);
			$em->flush();
		}*/

		return $fullFilename;
	}


	/**
	 * Save file to disc and to db
	 * @param  string $link Url
	 * @param  string $filename
	 * @param  string $withPersist
	 * @return string
	 */
	public function saveFileByExternalLink($link, $source, $filename = '', $withPersist = false)
	{
		$fileManager = $this->container->get('brands.file_manager');

		// Build save directory
		$uploadDir = $this->documentRoot . $this->config['directory'];
		// Check exist directory, and creation if not exist
		$fileManager->checkDirectory($uploadDir);

		$fileExt = $fileManager->getFileExt($link);

		if(!$filename) {
			$filename = $fileManager->generateFilename( $uploadDir, $fileExt );
		}
		$fullFilename = $filename;

		try {
			$file = $this->container->get('brands.utils')->curlExecute($link);
			if(!$file) {
				return false;
			}
			//$file = file_get_contents($link);
			file_put_contents($uploadDir . $filename, $file);
			$this->crop($uploadDir . $filename, $fullFilename, $source);
		} catch(\Exception $e) {
			return false;
		}



		return $fullFilename;
	}

	/**
	 *  Crop image
	 */
	public function crop($fullPath, $filename, $source) {
		$fileManager = $this->container->get('brands.file_manager');
		$fileManager->crop(
			file_get_contents($fullPath),
			$filename,
			$this->documentRoot,
			$this->config,
			$source
		);
	}

	/**
	 * Move file in current context
	 * @param  string $oldpath oldpath
	 * @param  string $filename
	 * @return boolean
	 */
	public function moveFileInCurrentContext($oldpath, $filename)
	{
		$uploadDir = $this->documentRoot.$this->getFolder();

		$fileManager = $this->container->get('brands.file_manager');
		$fileManager->checkDirectory($uploadDir);

		return rename($oldpath, $uploadDir.$filename);
	}

	/**
	 * Remove file and all thumbs from disc
	 * @param  Media $file
	 */
	public function remove($file)
	{
		$uploadDir = $this->documentRoot . $this->config['directory'];
		$filename = $file; //$file->getFilename();

		@unlink($uploadDir . '/' . $filename);

		if ( !isset($this->config['imageSizes']) || empty($this->config['imageSizes'])) {
			return;
		}
		$sizes = $this->config['imageSizes'];
		foreach ($sizes as $key => $value) {
			// Build save directory
			$uploadDir = $this->documentRoot . $this->config['directory'] . '/' . $key;

			@unlink($uploadDir . '/' . $filename);
		}
		return;
	}

	/**
	 * Get upload directory
	 */
	public function getFolder()
	{
		return isset($this->config['directory']) ? $this->config['directory'] : null;
	}

	/**
	 * Get document root
	 */
	public function getDocumentRoot()
	{
		return $this->documentRoot;
	}
}