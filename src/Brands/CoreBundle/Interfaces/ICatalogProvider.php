<?php

namespace Brands\CoreBundle\Interfaces;

interface ICatalogProvider
{
    /**
     * @return string ��� ��������
     */
    public function getName();

    /**
     * @return string ��� �������� ��������
     */
    public function getItemType();

    /**
     * @return string ��� ����, ������� ���������� ������������ � ������� �����
     */
    public function getKeyName();

    /**
     * @return string ��� ����, ������� ���������� ������������ � �������� ��������
     */
    public function getValueName();

    /**
     * @return array ������ ��������� ��������
     */
    public function getAllItems($params = array());

    /**
     * ���������� ������� �������� �� ����������� �����
     * @param mixed $key �������� ��������� ���� ��� ������ ��������
     * @param array $params �������������� ���������
     * @return mixed ������� ��������
     */
    public function getSingleItem($key, $params = array());
}