<?php

namespace Brands\CoreBundle\Handler;


use Brands\CoreBundle\Util\ExceptionWrapper;
use FOS\RestBundle\View\ExceptionWrapperHandlerInterface;


/**
 * @author: Toni Van de Voorde (toni [dot] vdv [AT] gmail [dot] com)
 */
class ExceptionWrapperHandler implements ExceptionWrapperHandlerInterface
{

	/**
	 * {@inheritdoc}
	 */
	public function wrap($data)
	{
		return new ExceptionWrapper($data);
	}
}