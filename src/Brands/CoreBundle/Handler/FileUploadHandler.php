<?php

namespace Brands\CoreBundle\Handler;

use Symfony\Component\DependencyInjection\ContainerInterface;

class FileUploadHandler
{
	private $container;
	public $fileLoader;

	/**
	 * Constructs a new instance
	 *
	 * @param ContainerInterface $container The container
	 */
	public function __construct(ContainerInterface $container)
	{
		$this->container = $container;
		$this->fileLoader = $this->container->get('brands.file_loader');
	}

	public function saveFile($file, $configurationName) {
		if($file) {
			$fileLoader = $this->container->get('brands.file_loader');
			$fileLoader->configure($configurationName);
			$fileErrors = $fileLoader->validate($file);
			if (count($fileErrors) > 0) {
				$errors = $fileErrors[0]->getMessage();
				return array('errors' => $errors);
			} else {
				$fileName = $fileLoader->save($file);
				return array('fileName' => $fileName);
			}
		}

		return array('errors' => 'File is empty');
	}

	public function removeFile($fileName, $configurationName) {
		if($fileName) {
			$fileLoader = $this->container->get('brands.file_loader');
			$fileLoader->configure($configurationName);

			$fileLoader->remove($fileName);
			return array('completed' => 1);
		}

		return array('errors' => 'File is empty');
	}

}