<?php
namespace Brands\CoreBundle\Repository;

use Doctrine\ORM\EntityRepository;

class CatalogItemRepository extends EntityRepository
{
	/**
	 * Загружает справочник по имени вместе со строками (CatalogItem)
	 * @param $name Имя справочника
	 * @return BrandsCoreBundle:CatalogItem|null
	 */
	public function findByName($name) {
		$qb =  $this->_em->createQueryBuilder();

		$qb
			->select('catalog, catalogItem')
			->from('BrandsCoreBundle:catalogItem', 'catalogItem')
			->leftJoin('catalogItem.catalog', 'catalog')
			->where('catalog.internalId = :name')
			->orderBy('catalogItem.sort', 'DESC')
		;
		$qb->setParameters(array('name' => $name));

		return $qb->getQuery()->getResult();
	}

	/**
	 * Возвращает только значение items
	 * @param $name Имя справочника
	 * @return BrandsCoreBundle:CatalogItem|null
	 */
	public function getItemsValue($name) {
		$qb =  $this->_em->createQueryBuilder();

		$qb
			->select('catalogItem.value, catalogItem.id')
			->from('BrandsCoreBundle:catalogItem', 'catalogItem')
			->leftJoin('catalogItem.catalog', 'catalog')
			->where('catalog.internalId = :name')
			->orderBy('catalogItem.sort', 'DESC')
		;
		$qb->setParameters(array('name' => $name));

		return $qb->getQuery()->getResult();
	}

	/*
	 * Возвращает запрос для построения справочника по имени
	 * @param string $name Наименование справочника
	 * @return QueryBuilder
	 */
	public function findByNameQuery($name) {
		return $this->createQueryBuilder('ld')
			->select(array('ld', 'li'))
			->leftJoin('ld.catalog', 'li')
			->where('li.name = :name')
			->setParameter(':name', 'faq_category');
	}

	/**
	 * Возвращает один элемент каталога по внутреннему ключу
	 * @param string $name Наименование каталога
	 * @param string $internalId Внутренний ключ
	 * @return mixed|null
	 */
	public function findOneByInternalId($name, $internalId) {
		try{
			$qb =  $this->_em->createQueryBuilder();
			$qb
				->select('catalog, catalogItem')
				->from('BrandsCoreBundle:CatalogItem', 'catalogItem')
				->leftJoin('catalogItem.catalog', 'catalog')
				->where('catalog.internalId = :name')
				->andWhere('catalogItem.internalId = :internalId')
				//->orderBy('ra.id', 'DESC')
			;
			$qb->setParameters(array('name' => $name, 'internalId' => $internalId));

			return $qb->getQuery()->getSingleResult();
		}
		catch(\Doctrine\ORM\NoResultException $e) {
			return null;
		}
	}
}