<?php
namespace Brands\CoreBundle\Repository;

use Doctrine\ORM\EntityRepository;

class CatalogRepository extends EntityRepository
{
    /**
     * Загружает справочник по имени вместе со строками (CatalogItem)
     * @param $name Имя справочника
     * @return FinseCoreBundle:Catalog|null
     */
    public function findOneByName($name) {
        try{
			$qb =  $this->_em->createQueryBuilder();
			$qb
				->select('catalog, catalogItem')
				->from('BrandsEmployeeApiBundle:Catalog', 'catalog')
				->leftJoin('catalog.catalogItem', 'catalogItem')
				->where('catalog.name = :name')
			;
			$qb->setParameters(array('name' => $name));
			return $qb->getQuery()->getSingleResult();
        }
        catch(\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
}