<?php
namespace Brands\CoreBundle\Twig;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CoreExtension extends \Twig_Extension
{
    /**
     * @var Symfony\Component\DependencyInjection\Container
     */
    private $container;

    /**
     * Constructs a new instance
     *
     * @param ContainerInterface $container The container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getFilters()
    {
        return array(
            'thumbnail' => new \Twig_Filter_Method($this, 'thumbnail'),
            'catalogItem' => new \Twig_Filter_Method($this, 'catalogItem'),
        );
    }

    public function thumbnail($imageName, $fileContext, $filterName)
    {
        $fileLoader = $this->container->get('brands.file_loader');
        $fileLoader->configure($fileContext);

        $imageFolder = $fileLoader->getFolder();
        $imagePath = $imageFolder.$imageName;

        $cacheManager = $this->container->get('liip_imagine.cache.manager');
        $thumb = $cacheManager->getBrowserPath($imagePath, $filterName);

        return $thumb;
    }

    public function catalogItem($catalogName, $catalogItemName)
    {
        $item = $this->container->get('catalog')->getSingleItem($catalogName, $catalogItemName, array('byInternalId' => true));

        return $item ? $item : null;
    }

    /*public function catalog($catalogName)
    {
        $items = $this->container->get('catalog')->getAllItems($catalogName);

        return $items ? $items : new ArrayCollection();
    }*/

    public function getName()
    {
        return 'core_extension';
    }
}