<?php

namespace Brands\CoreBundle\Listener;

use FOS\OAuthServerBundle\Tests\Functional\TestBundle\Entity\AccessToken;
use Brands\CoreBundle\Exception\AccessTokenException;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;


class KernelListener
{
	public function onKernelRequest(GetResponseEvent $event)
	{
		if($event->getRequest()->getMethod() == 'OPTIONS') {
			$response = new JsonResponse(array(), 200, array('Access-Control-Allow-Headers' => 'origin, Authorization, content-type, accept, X-Sso-Token, X-Current-Workspace-Id', 'Access-Control-Allow-Origin' => '*', 'Access-Control-Allow-Methods' => 'OPTIONS, POST, GET, PUT, DELETE'));
			$event->setResponse($response);
			return $event;
		}
	}

	public function onKernelResponse(FilterResponseEvent $event)
	{
		$responseHeaders = $event->getResponse()->headers;

		$responseHeaders->set('Access-Control-Allow-Headers', 'origin, content-type, accept, Authorization');
		$responseHeaders->set('Access-Control-Allow-Origin', '*');
		$responseHeaders->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE OPTIONS');
	}
}