<?php

namespace Brands\CoreBundle\Listener;

use FOS\OAuthServerBundle\Tests\Functional\TestBundle\Entity\AccessToken;
use Brands\CoreBundle\Exception\AccessTokenException;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;


class CoreErrorExceptionListener
{
	protected $container = null;

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
	}

	public function onKernelException(GetResponseForExceptionEvent $event)
	{
		// We get the exception object from the received event
		$exception = $event->getException();

		if($exception instanceof AccessTokenException) {
			$response = $this->container->get('brands.api.response')->generateBasicResponse(array('error' => $exception->getExceptionMessage()), $exception->getCode());
			$event->setResponse($response);
		}
	}
}