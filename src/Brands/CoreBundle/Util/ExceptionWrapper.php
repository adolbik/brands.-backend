<?php

namespace Brands\CoreBundle\Util;

/**
 * Wraps an exception into the FOSRest exception format
 */
class ExceptionWrapper
{
	/**
	 * @var string
	 */
	private $code;

	/**
	 * @var integer
	 */
	private $status_code;

	/**
	 * @var string
	 */
	private $status;

	/**
	 * @var string
	 */
	private $status_text;

	/**
	 * @var string
	 */
	private $message;

	/**
	 * @var mixed
	 */
	private $errors;

	/**
	 * @param array $data
	 */
	public function __construct($data)
	{
		$this->code = $data['status_code'];
		$this->message = $data['message'];
		if(isset($data['status_code']) && $data['status_code']) {
			$this->status_code = $data['status_code'];
		}
		if(isset($data['status']) && $data['status']) {
			$this->status = $data['status'];
		}
		if(isset($data['status_text']) && $data['status_text']) {
			$this->status_text = $data['status_text'];
		}

		if (isset($data['errors'])) {
			$this->errors = $data['errors'];
		}
	}
}
