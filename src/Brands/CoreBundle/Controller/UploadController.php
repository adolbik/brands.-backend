<?php

namespace Brands\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class UploadController extends Controller
{
    /**
     * Upload photo
     */
    public function uploadPhotosAction(Request $request) {
        $object = $request->get('object');

        if($object == 'goods-photo') {
            $photo = $request->files->get('images', array());
           	//$photo = isset($photo[0]) ? $photo[0] : null;
            //$photoFilter = 'form_preview';
        } else if ($object == 'shop-logo') {
            $photo = $request->files->get('logo', null);
            //$photoFilter = 'shop_logo';
        }

        $uploadHandler = $this->get('brands.file_upload_handler');
        $uploadResponse = $uploadHandler->saveFile($photo, 'temp');
        if(isset($uploadResponse['fileName']) && $fileName = $uploadResponse['fileName']) {
            $fileLoader = $this->get('brands.file_loader');
            $fileLoader->configure('temp');

            $imageFolder = $fileLoader->getFolder();
            $imagePath = $imageFolder.$fileName;

           // $cacheManager = $this->get('liip_imagine.cache.manager');
           // $thumb = $cacheManager->getBrowserPath($imagePath, $photoFilter);

            return new JsonResponse(array('completed' => 1, 'filename' => $fileName, 'path' => $imagePath));
        } else if(isset($uploadResponse['errors']) && $uploadResponse['errors']) {
            return new JsonResponse(array('completed' => 0, 'error' => $uploadResponse['errors']));
        }
    }
}
