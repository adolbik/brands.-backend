<?php

namespace Brands\CoreBundle\Components\Authentication\Handler;

use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Doctrine\ORM\EntityManager;

class LogoutSuccessHandler implements LogoutSuccessHandlerInterface
{

	protected $router;
	protected $em;

	public function __construct(Router $router, EntityManager $em)
	{
		$this->router = $router;
		$this->em = $em;
	}

	public function onLogoutSuccess(Request $request)
	{
		$clientId = $request->get('redirect_client_id');

		/*
			при logout, если указан redirect_client_id, то будем редиректить запрос так, чтобы пользователь оказался на
			форме логина и после авторизации вернулся в указанный апп
		*/
		if($clientId) {
			$client = $this->em->getRepository('BrandsOAuthBundle:Client')->find($clientId);
			if($client) {
				$urlClientId = $client->getId().'_'.$client->getRandomId();
				$clientRedirectUrl = (($uris = $client->getRedirectUris()) && isset($uris[0])) ? $uris[0] : null;

				if($urlClientId && $clientRedirectUrl) {
					$redirectUrl = $this->router->generate(
						'fos_oauth_server_authorize',
						array(
							'client_id'    => $urlClientId,
							'redirect_uri' => $clientRedirectUrl,
							'response_type' => 'code'
						)
					);

					$response = new RedirectResponse($redirectUrl);
					return $response;
				}
			}
		}

		// редиректим на форму логина
		$redirectUrl = $this->router->generate('fos_user_security_login');
		$response = new RedirectResponse($redirectUrl);

		return $response;
	}

}