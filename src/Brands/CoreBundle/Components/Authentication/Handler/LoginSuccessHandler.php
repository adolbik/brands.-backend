<?php

namespace Brands\CoreBundle\Components\Authentication\Handler;

use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationSuccessHandler;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Http\HttpUtils;

class LoginSuccessHandler extends DefaultAuthenticationSuccessHandler
{
	protected $session;

	public function __construct(HttpUtils $httpUtils)
	{
		parent::__construct($httpUtils, array());
		$this->providerKey = 'main';
	}

	public function onAuthenticationSuccess(Request $request, TokenInterface $token)
	{
		return $this->httpUtils->createRedirectResponse($request, $this->determineTargetUrl($request));
	}

	/**
	 * Builds the target URL according to the defined options.
	 *
	 * @param Request $request
	 *
	 * @return string
	 */
	protected function determineTargetUrl(Request $request)
	{
		if (null !== $this->providerKey && $targetUrl = $request->getSession()->get('_security.'.$this->providerKey.'.target_path')) {
			$request->getSession()->remove('_security.'.$this->providerKey.'.target_path');
		} else {
			$targetUrl = 'http://dev.brands.by';
		}

		return $targetUrl;
	}

}