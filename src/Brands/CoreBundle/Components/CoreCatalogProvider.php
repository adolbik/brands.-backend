<?php
namespace Brands\CoreBundle\Components;

use Brands\CoreBundle\Interfaces\ICatalogProvider;
use Symfony\Component\Config\Definition\Exception\Exception;

class CoreCatalogProvider implements ICatalogProvider {

    const EXCEPTION_KEY_IS_NOT_DEFINED      = 'Key __KEY__ is not defined';
    const EXCEPTION_GETTER_VALUE_IS_NULL   = 'Getter __GETTER__ returned NULL';

    // EntityManager
    private $_em = null;

    // Наименование каталога
    private $_name = '';

    public function __construct($name, $em) {
        $this->_em = $em;
        $this->_name = $name;
    }

    /**
     * @return string Имя каталога
     */
    public function getName() {
        return 'CoreCatalog';
    }

    /**
     * @return string Тип элемента каталога
     */
    public function getItemType() {
        return 'BrandsCoreBundle:CatalogItem';
    }

    /**
     * @return string Имя поля, которое необходимо использовать в качесве ключа
     */
    public function getKeyName() {
        return 'id';
    }

    /**
     * @return string Имя поля, которое необходимо использовать в качестве значения
     */
    public function getValueName() {
        return 'value';
    }

    /**
     * @return array Массив элементов каталога
     */
    public function getAllItems($params = array()) {
        $type = 'object';
        if(isset($params['type'])) {
            $type = $params['type'];
        }

        $key = 'id';
        if(isset($params['key'])){
            $key = $params['key'];
        }
        $getter = 'get' . ucfirst($key);

        $catalogRep = $this->_em->getRepository('BrandsCoreBundle:CatalogItem');
        $catalog = array();
        foreach($catalogRep->findByName($this->_name) as $catalogItem) {
            if(method_exists($catalogItem, $getter)) {
                $key = $catalogItem->$getter();
                if (is_null($key)){
                    throw new Exception(str_replace('__GETTER__', $getter, self::EXCEPTION_GETTER_VALUE_IS_NULL));
                }
            } else {
                throw new Exception(str_replace('__KEY__', $key, self::EXCEPTION_KEY_IS_NOT_DEFINED));
            }

            switch($type) {
                case 'object':
                    $catalog[$key] = $catalogItem;
                    break;
                case 'list_box_array':
                    $catalog[$key] = $catalogItem->getValue();
                    break;
            }
        }

        return $catalog;
    }

	/**
     * @return array Массив значений элементов каталога (internal_id, value)
     */
    public function getAllItemsValue($params = array()) {
        $type = 'object';
        if(isset($params['type'])) {
            $type = $params['type'];
        }

        $key = 'id';
        if(isset($params['key'])){
            $key = $params['key'];
        }
        $getter = 'get' . ucfirst($key);

        $catalogRep = $this->_em->getRepository('BrandsCoreBundle:CatalogItem');
        $catalog = array();
        foreach($catalogRep->getItemsValue($this->_name) as $catalogItem) {
            $catalog[] = $catalogItem;
        }

        return $catalog;
    }

    /**
     * @param mixed $key Значение ключевого поля для поиска элемента
     * @return mixed Элемент каталога
     */
    public function getSingleItem($key, $params = array()) {
        $catalog = $this->_em->getRepository('BrandsCoreBundle:CatalogItem');
        if(isset($params['byInternalId']) && ($params['byInternalId'] === true)) {
            $catalogItem = $catalog->findOneByInternalId($this->_name, $key);
        }
        else {
            $catalogItem = $catalog->findOneById($key);
        }
        if (!$catalogItem) {
            return null;
        }
        return $catalogItem;
    }
}