<?php

namespace Brands\CoreBundle\Components;

use Brands\CoreBundle\Interfaces\ICacheDependencyGenerator;

class CacheEntityDependencyGenerator implements ICacheDependencyGenerator {
    private $_em = null;
    private $_entityNames = null;

    public function __construct($em, $entityNames) {
        $this->_em = $em;
        $this->_entityNames = $entityNames;
    }

    /**
     * ��������� �����������
     */
    public function generateDependency() {
        $tables = array();
        foreach($this->_entityNames as $entityName) {
            $tables[] = $this->_em->getClassMetadata($entityName)->getTableName();
        }

        return $this->_em->getRepository('BrandsCoreBundle:Cache')->findLastModifiedByNames($tables);
    }
}