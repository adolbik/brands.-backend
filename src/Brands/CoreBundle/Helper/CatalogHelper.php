<?php

namespace Brands\CoreBundle\Helper;

/**
 * Класс для хранения констант, связанных с каталог-айтэм
 */

class CatalogHelper
{
	const CATALOG_SIZES = 'goods_size';
	const CATALOG_CURRENCY = 'currency';

	const CATALOG_ITEM_SUBSCRIPTION_PARSER = 'parser';
	const CATALOG_ITEM_SUBSCRIPTION_SHOP = 'shop';
	const CATALOG_ITEM_SUBSCRIPTION_DEALER = 'dealer';
}