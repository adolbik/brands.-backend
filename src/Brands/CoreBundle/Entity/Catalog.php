<?php

namespace Brands\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections as Collections;

/**
 * Catalog
 *
 * @ORM\Entity
 * @ORM\Table(name="catalog")
 * @ORM\Entity(repositoryClass="Brands\CoreBundle\Repository\CatalogRepository")
 * @ORM\HasLifecycleCallbacks()
*/
class Catalog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false, unique=true)
     */
    protected $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", nullable=false, unique=true)
	 */
	protected $internalId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     */
    protected $description;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    protected $active = 1;

    /**
     * @var Collections\ArrayCollection|CatalogItem
     *
     * @ORM\OneToMany (targetEntity="CatalogItem", mappedBy="catalog")
     */
    protected $catalogItem;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->catalogItem = new \Doctrine\Common\Collections\ArrayCollection();
    }

	public function __toString() {
		return $this->name;
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Catalog
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set internalId
     *
     * @param string $internalId
     * @return Catalog
     */
    public function setInternalId($internalId)
    {
        $this->internalId = $internalId;

        return $this;
    }

    /**
     * Get internalId
     *
     * @return string 
     */
    public function getInternalId()
    {
        return $this->internalId;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Catalog
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Catalog
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Add catalogItem
     *
     * @param \Brands\CoreBundle\Entity\CatalogItem $catalogItem
     * @return Catalog
     */
    public function addCatalogItem(\Brands\CoreBundle\Entity\CatalogItem $catalogItem)
    {
        $this->catalogItem[] = $catalogItem;

        return $this;
    }

    /**
     * Remove catalogItem
     *
     * @param \Brands\CoreBundle\Entity\CatalogItem $catalogItem
     */
    public function removeCatalogItem(\Brands\CoreBundle\Entity\CatalogItem $catalogItem)
    {
        $this->catalogItem->removeElement($catalogItem);
    }

    /**
     * Get catalogItem
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCatalogItem()
    {
        return $this->catalogItem;
    }
}
