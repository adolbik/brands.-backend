<?php

namespace Brands\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections as Collections;
use JMS\Serializer\Annotation\ExclusionPolicy,
	JMS\Serializer\Annotation\Exclude;

/**
 * CatalogItem
 *
 * @ORM\Entity
 * @ORM\Table(name="catalog_item",
 *              uniqueConstraints={@ORM\UniqueConstraint(name="internal_id", columns={"catalog_id", "internal_id"})})
 * @ORM\Entity(repositoryClass="Brands\CoreBundle\Repository\CatalogItemRepository")
 */
class CatalogItem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="catalog_id", type="integer", nullable=false)
     */
    protected $catalogId;

    /**
     * @var integer
     *
     * @ORM\Column(name="internal_id", type="string", length=255, nullable=false,
     *              options={"comment"="Внутренний идентификатор записи в справочнике"})
     */
    protected $internalId;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $value;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable = true)
     */
    protected $extra;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     */
    protected $sort;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    protected $active;

    /**
     * @ORM\ManyToOne(targetEntity="Catalog", inversedBy="catalogItem")
     * @ORM\JoinColumn(name="catalog_id", referencedColumnName="id", onDelete="CASCADE")
	 * @Exclude
     */
    protected $catalog;


	public function __construct() {
		$this->sort = 1;
		$this->active = 1;
	}

	public function __toString() {
		return $this->value;
	}

	public function getExtraAsArray(){
        $extra = array();
        if ($this->extra) {
            $extra = json_decode($this->extra);
        }

        return $extra;
	}

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set catalogId
     *
     * @param integer $catalogId
     * @return CatalogItem
     */
    public function setCatalogId($catalogId)
    {
        $this->catalogId = $catalogId;

        return $this;
    }

    /**
     * Get catalogId
     *
     * @return integer
     */
    public function getCatalogId()
    {
        return $this->catalogId;
    }

    /**
     * Set internalId
     *
     * @param string $internalId
     * @return CatalogItem
     */
    public function setInternalId($internalId)
    {
        $this->internalId = $internalId;

        return $this;
    }

    /**
     * Get internalId
     *
     * @return string
     */
    public function getInternalId()
    {
        return $this->internalId;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return CatalogItem
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set extra
     *
     * @param string $extra
     * @return CatalogItem
     */
    public function setExtra($extra)
    {
        $this->extra = $extra;

        return $this;
    }

    /**
     * Get extra
     *
     * @return string
     */
    public function getExtra()
    {
        return $this->extra;
    }

    /**
     * Set sort
     *
     * @param integer $sort
     * @return CatalogItem
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return integer
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return CatalogItem
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set catalog
     *
     * @param \Brands\CoreBundle\Entity\Catalog $catalog
     * @return CatalogItem
     */
    public function setCatalog(\Brands\CoreBundle\Entity\Catalog $catalog = null)
    {
        $this->catalog = $catalog;

        return $this;
    }

    /**
     * Get catalog
     *
     * @return \Brands\CoreBundle\Entity\Catalog
     */
    public function getCatalog()
    {
        return $this->catalog;
    }
}
