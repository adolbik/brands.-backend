<?php
namespace Brands\CoreBundle\Service;
/**
 * Created by PhpStorm.
 * User: alexandr
 * Date: 12.12.13
 * Time: 15:18
 */

class Mailer
{
	private $mailer;

	/**
	 * Constructs a new instance
	 *
	 * @param ContainerInterface $container The container
	 */
	public function __construct($mailer)
	{
		$this->mailer = $mailer;
	}

	/**
	 * @param        $template
	 * @param        $args
	 * @param        $subject
	 * @param        $to
	 * @param        $file
	 * @param string $from
	 *
	 * @return string
	 * @throws \ErrorException
	 */
	public function sendEmail($mail, $template, $args, $attached, $subject, $to, $from = '', $file = null){
		$mailer = $this->mailer;
		$renderedTemplate = $this->renderTemplate($template, $args);
		//$renderedTemplate = nl2br($renderedTemplate);

		$from = $from ? $from : 'noreply@apalon.com';
		$message = $renderedTemplate;
		$subject = strip_tags($subject);

		$mail
			->setSubject($subject)
			->setFrom(array($from => 'Brands.by'))
			->setTo($to)
		;

		if($attached) {
			foreach($attached as $k => $att) {
				$attached[$k] = $mail->embed(\Swift_Image::fromPath($att));
			}

			$message = $this->renderTemplate($message, $attached);
		}

		$mail->setBody($message,'text/html');

		return $mailer->send($mail);
	}

	public function renderTemplate($template, $args){
		$twig = new \Twig_Environment(new \Twig_Loader_String());
		$rendered = $twig->render($template, $args);

		return $rendered;
	}

}