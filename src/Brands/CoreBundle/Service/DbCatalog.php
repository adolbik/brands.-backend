<?php

namespace Brands\CoreBundle\Service;

class DbCatalog extends BaseCatalog
{
    private $_em;

    public function __construct($em, $catalogs) {
        $this->_em = $em;
        parent::__construct($catalogs);
    }

    protected function getCatalogProvider($name) {
        if(!isset(self::$_providers[$name]) || is_null(self::$_providers[$name])) {
			if(isset($this->_catalogs[$name]) && !is_null($this->_catalogs[$name])) {
				$providerClass = $this->_catalogs[$name];
			} else {
				$providerClass = $this->_catalogs['default'];
			}
            self::$_providers[$name] = new $providerClass($name, $this->_em);
        }

        return self::$_providers[$name];
    }

    public function getCatalogListValue($values) {
        $result = array();

        foreach($values as $v) {
            $result[] = $v->getValue();
        }

        return $result;
    }
}
