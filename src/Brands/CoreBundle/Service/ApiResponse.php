<?php

namespace Brands\CoreBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Config\Definition\Exception\Exception;
use FOS\RestBundle\View\View;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\JsonResponse;

class ApiResponse
{
	private $container;

	/**
	 * Constructs a new instance
	 *
	 * @param ContainerInterface $container The container
	 */
	public function __construct(ContainerInterface $container)
	{
		$this->container = $container;
	}

	public function generateResponse($data, $code, $groups = array()) {
		if(!$data) {
			$data = array();
		}
		if(is_array($data)) {
			$data['code'] = $code;
		} else {
			$currentData = $data;
			$data = array();
			array_push($data, $currentData, array('code' => $code));
		}

		$response = View::create($data, $code);
		$groups = array_merge($groups, array('Default'));
		if($groups) {
			$response->setSerializationContext(SerializationContext::create()->setGroups($groups));
		}

		$response->setHeaders(array('Access-Control-Allow-Headers' => 'origin, content-type, accept, X-Sso-Token, Authorization', 'Access-Control-Allow-Origin' => '*', 'Access-Control-Allow-Methods' => 'OPTIONS, POST, GET, PUT, DELETE'));
		$response->setStatusCode($code);
		$response->setFormat('json');

		return $response;
	}

	public function generateBasicResponse($data, $code) {
		if(!$data) {
			$data = array();
		}
		if(is_array($data)) {
			$data['code'] = $code;
		}

		$response = new JsonResponse($data, $code, array('Access-Control-Allow-Headers' => 'origin, content-type, accept, Authorization', 'Access-Control-Allow-Origin' => '*', 'Access-Control-Allow-Methods' => 'POST, GET, PUT, DELETE, OPTIONS'));

		return $response;
	}
}