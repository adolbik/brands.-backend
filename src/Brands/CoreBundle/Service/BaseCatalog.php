<?php

namespace Brands\CoreBundle\Service;

abstract class BaseCatalog
{
    protected $_catalogs = array();
    protected static $_providers = array();

    protected function __construct($catalogs) {
        $this->_catalogs = $catalogs;
    }

    abstract protected function getCatalogProvider($name);

    public function getItemType($name) {
        $provider = $this->getCatalogProvider($name);

        if(!is_null($provider)) {
            return $provider->getItemType();
        }

        return false;
    }

    public function getValueName($name) {
        $provider = $this->getCatalogProvider($name);

        if(!is_null($provider)) {
            return $provider->getValueName();
        }

        return false;
    }

    public function getAllItems($name, $params = array()) {
        $provider = $this->getCatalogProvider($name);

        if(!is_null($provider)) {
            return $provider->getAllItems($params);
        }

        return false;
    }

	public function getAllItemsValue($name, $params = array()) {
        $provider = $this->getCatalogProvider($name);

        if(!is_null($provider)) {
            return $provider->getAllItemsValue($params);
        }

        return false;
    }

    public function getSingleItem($name, $key, $params = array()) {
        $provider = $this->getCatalogProvider($name);

        if(!is_null($provider)) {
            return $provider->getSingleItem($key, $params);
        }

        return false;
    }
}
