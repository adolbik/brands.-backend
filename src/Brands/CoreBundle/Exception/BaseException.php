<?php

namespace Brands\CoreBundle\Exception;


class BaseException extends \Exception {

	protected $code = 404;
	protected $errorType = '';
	protected $message = null;
	protected $innerCode = -2000;
	protected $innerResult = [];


	public function getInnerCode()
	{
		return $this->innerCode;
	}


	public function setInnerCode($code)
	{
		$this->innerCode = $code;
	}


	public function getResult()
	{
		return $this->innerResult;
	}


	public function setResult($result)
	{
		$this->innerResult = $result;
	}

	public function getErrorType()
	{
		return $this->errorType;
	}

	public function setErrorType($errorType)
	{
		$this->errorType = $errorType;
	}

	public function getExceptionMessage()
	{
		return $this->message;
	}

	public function setExceptionMessage($message)
	{
		$this->message = $message;
	}

}