<?php

namespace Brands\GoodsApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Doctrine\ORM\EntityRepository;
use Brands\CoreBundle\Helper\CatalogHelper;
use Brands\GoodsApiBundle\Entity\GoodsPhoto;
use Brands\AdminBundle\Entity\CategoryToUser;
use Doctrine\ORM\EntityManager;
use Brands\AdminBundle\Service\Category;

class GoodsType extends AbstractType
{
	protected $_fileUploadHandler;
	protected $_categoryService;
	protected $_em;

	public function __construct(EntityManager $em, $fileUploadHandler, Category $categoryService) {
		$this->_fileUploadHandler = $fileUploadHandler;
		$this->_categoryService = $categoryService;
		$this->_em = $em;
	}

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('price')
            ->add('description')
			->add('currency', 'entity', array(
				  'class' => 'BrandsCoreBundle:CatalogItem',
				  'query_builder' => function(EntityRepository $er) {
						  return $er->createQueryBuilder('i')
							  ->leftJoin('i.catalog', 'catalog')
							  ->andWhere('catalog.internalId = :cataloginternalId')
							  ->orderBy('i.sort', 'ASC')
							  ->setParameter('cataloginternalId', CatalogHelper::CATALOG_CURRENCY)
							  ;
					  },
				  'expanded'	=> true,
				  'required'  => true,
				  'multiple' => false
			))
            //->add('photos', 'text', array('data_class' => null, 'required' => false))
			->add('sizes', 'entity', array(
			  	'class' => 'BrandsCoreBundle:CatalogItem',
			  	'query_builder' => function(EntityRepository $er) {
					return $er->createQueryBuilder('i')
						->leftJoin('i.catalog', 'catalog')
						->andWhere('catalog.internalId = :cataloginternalId')
						->orderBy('i.sort', 'ASC')
						->setParameter('cataloginternalId', CatalogHelper::CATALOG_SIZES)
						;
				},
				'expanded'	=> true,
				'required'  => false,
				'multiple' => true
		 	))
			->add('category', 'entity', array(
				 'class' => 'BrandsAdminBundle:Category',
				 'required'  => true,
				 'multiple' => false,
				 'property' => 'name'
			))
			->add('mainImage', 'hidden', array('required' => false, 'mapped' => false))
			->add('inStock')
			->add('isForMale')
			->add('isForFemale')
			->add('inStock')
			->add('images', 'collection', array(
				'type' => 'text',
				'mapped' => false,
				'options' => array(),
				'required' => false,
				'allow_add' => true,
				'allow_delete' => true
			))
        ;

		$builder->addEventListener(FormEvents::POST_SUBMIT , array($this, 'attachPhotos'));
		$builder->addEventListener(FormEvents::POST_SUBMIT , array($this, 'calculateCategoryAllGoods'));
		$builder->addEventListener(FormEvents::POST_SUBMIT , array($this, 'calculateCategoryUserGoods'));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Brands\GoodsApiBundle\Entity\Goods',
			'csrf_protection'   => false
        ));
    }

	public function attachPhotos(FormEvent $event) {
		$form = $event->getForm();
		$goods = $form->getData();
		$mainPhoto = $form->get('mainImage')->getData();

		$oldImages = $goods->getPhotos();
        $oldImagesArr = array();
        foreach($goods->getPhotos() as $p) {
            $oldImagesArr[] = $p->getSrc();
        }

		/* новые загруженные фотки */
		$photos = $form->get('images')->getData();
		$fileLoader = $this->_fileUploadHandler->fileLoader;

		$fileLoader->configure('temp');
		$tempFolder = $fileLoader->getFolder();

		$fileLoader->configure('goods_photo');

		// удаляем старые картинки
		if($form->isValid()) {
			foreach($oldImages as $oldImage) {
				if(!in_array($oldImage->getSrc(), $photos)) {
					$goods->removePhoto($oldImage);
					$this->_fileUploadHandler->removeFile($oldImage->getSrc(), 'goods_photo');
				} else {
					$oldImage->setIsMain($mainPhoto == $oldImage->getSrc() ? 1 : 0);
				}
			}
		}

		$addedPhotos = 0;
		foreach($photos as $photo) {
            if(in_array($photo, $oldImagesArr)) {
                continue;
            }

			$oldPath = $fileLoader->getDocumentRoot().$tempFolder.$photo;

			if(file_exists($oldPath)) {
				if($form->isValid()) {
					/* перемещаем файл в постоянную папку */
					$fileLoader->moveFileInCurrentContext($oldPath, $photo);
				}

				/*  Создаем записи в базе */
				$goodsPhoto = new GoodsPhoto();
				$goodsPhoto->setSrc($photo);
				$goodsPhoto->setGoods($goods);
				$goodsPhoto->setIsMain($mainPhoto == $photo ? 1 : 0);

				$goods->addPhoto($goodsPhoto);
				$addedPhotos++;
			}
		}
	}

	public function calculateCategoryAllGoods(FormEvent $event) {
		$form = $event->getForm();
		$goods = $form->getData();

		$category = $goods->getCategory();
		$this->_categoryService->setCategoryGoodsCount($category, $goods->getId() ? false : true);
	}

	public function calculateCategoryUserGoods(FormEvent $event) {
		$form = $event->getForm();
		$goods = $form->getData();

		$category = $goods->getCategory();
		$this->_categoryService->setCategoryUserGoodsCount($category, $goods->getOwner(), $goods->getId() ? false : true);
	}


    public function getName()
    {
        return 'goods';
    }
}
