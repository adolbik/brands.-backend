<?php

namespace Brands\GoodsApiBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\FOSRestController;

use Symfony\Component\Form\FormError;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Brands\CoreBundle\Helper\CatalogHelper;

use Assetic\Exception\Exception;

class CategoryApiController extends BaseController
{
	/**
	 * List of categories
	 *
	 * @ApiDoc(
	 *  resource=true,
	 *  description="List of categories",
	 *  sorting={}
	 * )
	 *
	 * @Rest\View
	 */
	public function listAction(Request $request)	{
		$filter = $request->get('filter');
		$order = $request->get('order');
		$firstResult = $request->get('firstResult', 0);
		$offset = $request->get('offset', 1000);

		$em = $this->getDoctrine()->getManager();

		$categories = $em->getRepository('BrandsAdminBundle:Category')->getTree();
		//$categories = $em->getRepository('BrandsAdminBundle:Category')->findAll();

		return $this->get('brands.api.response')->generateResponse(array('categories' => $categories), Response::HTTP_OK, array('only_category'));
	}

	/**
	 * List of goods for category
	 *
	 * @ApiDoc(
	 *  resource=true,
	 *  description="List of goods for category",
	 *  sorting={}
	 * )
	 *
	 * @Rest\View
	 */
	public function listGoodsAction(Request $request)	{
		$categoryId = $request->get('categoryId');
		$em = $this->getDoctrine()->getManager();
		$user = $this->getUser();

		$moderatedGoods = $em->getRepository('BrandsGoodsApiBundle:Goods')->findGoodsByCategory($categoryId, $user->getId());//findModeratedByCategory($categoryId, $user->getId());
		$notModeratedGoods = array();//$em->getRepository('BrandsGoodsApiBundle:Goods')->findNotModeratedByCategory($categoryId, $user->getId());
		$rejectedGoods = array(); //$em->getRepository('BrandsGoodsApiBundle:Goods')->findRejectedByCategory($categoryId, $user->getId());
		foreach($moderatedGoods as $g) {
			$this->get('brands.goods')->addEntityPhotosPath($g, 'goods_photo');
		}
        /*foreach($notModeratedGoods as $g) {
			$this->get('brands.goods')->addEntityPhotosPath($g, 'goods_photo');
		}
        foreach($rejectedGoods as $g) {
			$this->get('brands.goods')->addEntityPhotosPath($g, 'goods_photo');
		}*/

		return $this->get('brands.api.response')->generateResponse(array('moderatedGoods' => $moderatedGoods, 'notModeratedGoods' => $notModeratedGoods, 'rejectedGoods' => $rejectedGoods), Response::HTTP_OK);
	}

	/**
	 * Info about category goods
	 *
	 * @ApiDoc(
	 *  resource=true,
	 *  description="Info about category goods",
	 *  sorting={}
	 * )
	 *
	 * @Rest\View
	 */
	public function createGoodsInfoAction(Request $request)	{
		$categoryId = $request->get('categoryId');
		$em = $this->getDoctrine()->getManager();

		$category = $em->getRepository('BrandsAdminBundle:Category')->findWithRelated($categoryId);
		$sizes = $category->getSizes();

		if(!count($sizes)) {
			$sizes = $this->get('brands.category')->getParentCategoriesSizes($categoryId);
		}

		$currency = $this->get('brands.catalog')->getAllItems(CatalogHelper::CATALOG_CURRENCY);

		return $this->get('brands.api.response')->generateResponse(array('sizes' => $sizes, 'currency' => $currency, 'category' => $category), Response::HTTP_OK);
	}

	/**
	 * Return entity with thumbnails path
	 * @var $entity
	 * @var string $fileContext FileLoader context
	 */
	public function addEntityPhotosPath($entity, $fileContext) {
		$baseUrl = 'http://'.$this->get('request')->getHttpHost();

		if(!is_array($entity)) {
			$entities[] = $entity;
		} else {
			$entities = $entity;
		}
		foreach($entities as $e) {
			if(!$e || !$e->getPhotos()) {
				continue;
			}
			$photos = $e->getPhotos();

			$fileLoader = $this->get('brands.file_loader');
			$fileLoader->configure($fileContext);
			$imageFolder = $fileLoader->getFolder();

			foreach($photos as $photo) {
				$imageOriginal = ($photo && $photo->getSrc()) ?  $imageFolder.$photo->getSrc() : '';
				if($imageOriginal) {
					$cacheManager = $this->container->get('liip_imagine.cache.manager');
					$srcPath_54x50 = $cacheManager->getBrowserPath($imageOriginal, '54x50');
					$originalName = $photo->getSrc();

					$arr = array('original_name' => $originalName, '54x50' => $srcPath_54x50, 'path' => $baseUrl.$imageOriginal);
				} else {
					$arr = array();
				}

				$photo->setExtra($arr);
			}
		}

		return $entity;
	}
}
