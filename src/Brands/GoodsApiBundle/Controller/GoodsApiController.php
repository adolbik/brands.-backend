<?php

namespace Brands\GoodsApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\FOSRestController;

use Symfony\Component\Form\FormError;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use Brands\GoodsApiBundle\Entity\Goods;
use Brands\GoodsApiBundle\Form\GoodsType;

class GoodsApiController extends BaseController
{

	/**
	 * Create a new goods
	 *
	 * @ApiDoc(
	 *  resource=true,
	 *  description="Create a new goods",
	 *  input="Brands\GoodsApiBundle\Form\GoodsType"
	 * )
	 * @Rest\View
	 */
	public function createAction(Request $request)	{
		$goods = new Goods();
		$goods->setOwner($this->getUser());

		return $this->processForm($goods);
	}

	/**
	 * Edit a goods
	 *
	 * @ApiDoc(
	 *  resource=true,
	 *  description="Edit a goods",
	 *  input="Brands\GoodsApiBundle\Form\GoodsType",
	 * 	requirements={
	 *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Goods id"}
	 *  }
	 * )
	 *
	 * @Rest\View
	 */
	public function editAction($id)	{
		$user = $this->getUser();

        if(!(($goods = $this->getAndCheckGoods($id, $user)) instanceof Goods)) {
            return $goods;
        }

		return $this->processForm($goods);
	}

	/**
	 * Get a goods info
	 *
	 * @ApiDoc(
	 *  resource=true,
	 *  description="Get a goods info",
	 * 	requirements={
	 *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Goods id"}
	 *  }
	 * )
	 *
	 * @Rest\View
	 */
	public function getAction($id)	{
		$em = $this->getDoctrine()->getManager();
		$user = $this->getUser();

        if(!(($goods = $this->getAndCheckGoods($id, $user)) instanceof Goods)) {
            return $goods;
        }

		$this->get('brands.goods')->addEntityPhotosPath($goods, 'goods_photo');
        // inc views
        $goods->setViews($goods->getViews() + 1);
        $em->flush();

		return $this->get('brands.api.response')->generateResponse(array('goods' => $goods), Response::HTTP_OK);
	}

	/**
	 * Delete a goods
	 *
	 * @ApiDoc(
	 *  resource=true,
	 *  description="Delete a goods",
	 * 	requirements={
	 *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Goods id"}
	 *  }
	 * )
	 *
	 * @Rest\View
	 */
	public function deleteAction($id)	{
		$user = $this->getUser();

        if(!(($goods = $this->getAndCheckGoods($id, $user)) instanceof Goods)) {
            return $goods;
        }

		$this->get('brands.goods')->removeGoods($goods);

		return $this->get('brands.api.response')->generateResponse(array(), Response::HTTP_OK);
	}

	/**
	 * Save / Create employee
	 *
	 *
	 * @Rest\View
	 */
	public function processForm(Goods $goods) {
		$request = $this->get('request');
		$statusCode = $goods->isNew() ? Response::HTTP_CREATED : Response::HTTP_OK;

		$form = $this->createForm(new GoodsType($this->get('doctrine')->getManager(), $this->get('brands.file_upload_handler'), $this->get('brands.category')), $goods);
		$form->handleRequest($request);

		if ($form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			if($isNew = $goods->isNew()) {
				$em->persist($goods);
			}
			$em->flush();

			$responseData = array();
			if (Response::HTTP_CREATED === $statusCode) {
				$responseData['id'] = $goods->getId();
			}

			return $this->get('brands.api.response')->generateResponse($responseData, $statusCode);
		}

		return $this->get('brands.api.response')->generateResponse(array('form' => $form), Response::HTTP_BAD_REQUEST);
	}
}
