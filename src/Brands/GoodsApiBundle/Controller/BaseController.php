<?php

namespace Brands\GoodsApiBundle\Controller;

use Brands\CoreBundle\Controller\BaseController as CoreBaseController;
use Brands\GoodsApiBundle\Entity\Goods;

class BaseController extends CoreBaseController
{
    /**
     * @param $id
     * @param $user
     * @return bool|\FOS\RestBundle\View\View
     */
    public function getAndCheckGoods($id, $user) {
        $em = $this->getDoctrine()->getManager();

        $goods = $em->getRepository('BrandsGoodsApiBundle:Goods')->findOneBy(array('id' => $id, 'owner' => $user));
        return $this->checkGoods($goods);
    }

    /**
     * @param Goods $goods
     * @return bool|\FOS\RestBundle\View\View
     */
    public function checkGoods(Goods $goods) {
        if(!$goods) {
            $responseData = array('error' => 'Goods not found');
            return $this->get('brands.api.response')->generateResponse($responseData, Response::HTTP_NOT_FOUND);
        }

        return $goods;
    }
}
