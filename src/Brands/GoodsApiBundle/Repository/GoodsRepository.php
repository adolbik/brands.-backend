<?php
namespace Brands\GoodsApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class GoodsRepository extends EntityRepository
{
	/**
	 * @param      $categoryId
	 * @param null $ownerId
	 *
	 * @return array|null
	 */
	public function findGoodsByCategory($categoryId, $ownerId = null) {
		$qb = $this->findByCategoryQB($categoryId, $ownerId);

		try{
			$result = $qb->getQuery()->getResult();
			return $result;

		} catch(\Doctrine\ORM\NoResultException $e) {
			return null;
		}
	}

	/**
	 * @param      $categoryId
	 * @param null $ownerId
	 *
	 * @return array|null
	 */
	public function findModeratedByCategory($categoryId, $ownerId = null) {
		$qb = $this->findByCategoryQB($categoryId, $ownerId);

        $qb->andWhere('goods.isModerated = 1');

		try{
			$result = $qb->getQuery()->getResult();
			return $result;

		} catch(\Doctrine\ORM\NoResultException $e) {
			return null;
		}
	}

    /**
	 * @param      $categoryId
	 * @param null $ownerId
	 *
	 * @return array|null
	 */
	public function findNotModeratedByCategory($categoryId, $ownerId = null) {
		$qb = $this->findByCategoryQB($categoryId, $ownerId);

        $qb->andWhere('goods.isModerated = 0');
        $qb->andWhere('goods.isRejected = 0');

		try{
			$result = $qb->getQuery()->getResult();
			return $result;

		} catch(\Doctrine\ORM\NoResultException $e) {
			return null;
		}
	}

    /**
	 * @param      $categoryId
	 * @param null $ownerId
	 *
	 * @return array|null
	 */
	public function findRejectedByCategory($categoryId, $ownerId = null) {
		$qb = $this->findByCategoryQB($categoryId, $ownerId);

        $qb->andWhere('goods.isRejected = 1');

		try{
			$result = $qb->getQuery()->getResult();
			return $result;

		} catch(\Doctrine\ORM\NoResultException $e) {
			return null;
		}
	}

    public function findByCategoryQB($categoryId, $ownerId = null) {
        $qb = $this->createQueryBuilder('goods');
        $qb->select('goods, category, photos, sizes')
            ->leftJoin('goods.category', 'category')
            ->leftJoin('goods.photos', 'photos')
            ->leftJoin('goods.sizes', 'sizes')
            ->where('goods.category = :categoryId')
            ->andWhere('categoryToUser.user = :ownerId')
        ;

        $qb->setParameter('categoryId', $categoryId);

		if($ownerId) {
			$qb->leftJoin('category.categoryToUser', 'categoryToUser');
			$qb->andWhere('categoryToUser.user = :ownerId');
			$qb->andWhere('goods.owner = :ownerId');

			$qb->addSelect('categoryToUser');
			$qb->setParameter('ownerId', $ownerId);
		}

        return $qb;
    }

	/**
	 * @param      $categoryId
	 * @param null $ownerId
	 *
	 * @return array|null
	 */
	/*public function getCountCategoryGoods($categoryId, $ownerId = null) {
		$qb = $this->createQueryBuilder('goods');
		$qb->select('COUNT(goods.id) as cnt')
			->where('goods.category = :categoryId')
		;

		if($ownerId) {
			$qb
				->andWhere('goods.owner = :ownerId')
			   	->setParameter('ownerId', $ownerId);
		}

		$qb->setParameter('categoryId', $categoryId);

		try{
			$result = $qb->getQuery()->getArrayResult();

			return isset($result[0]['cnt']) ? $result[0]['cnt'] : 0;

		} catch(\Doctrine\ORM\NoResultException $e) {
			return null;
		}
	}*/

	/**
	 * @param      $categoryId
	 * @param null $ownerId
	 *
	 * @return array|null
	 */
	public function getCountCategoryGoods($categoryId, $ownerId = null) {
		$qb = $this->createQueryBuilder('goods');
		$qb->select('COUNT(goods.id) as cnt')
			->leftJoin('goods.category', 'category')
			->where('(category.materializedPath LIKE :path OR category.id = :categoryId)')
		;

		if($ownerId) {
			$qb
				->andWhere('goods.owner = :ownerId')
			   	->setParameter('ownerId', $ownerId);
		}

		$qb->setParameter('path', '%/'.$categoryId.'%');
		$qb->setParameter('categoryId', $categoryId);

		try{
			$result = $qb->getQuery()->getArrayResult();

			return isset($result[0]['cnt']) ? $result[0]['cnt'] : 0;

		} catch(\Doctrine\ORM\NoResultException $e) {
			return null;
		}
	}

    /**
     * Находим товары, которые еще не модерировались
     * @return array|null
     */
    public function findNotModeratedGoods() {
        $qb = $this->createQueryBuilder('goods');
        $qb->select('goods')
            ->where('goods.isModerated = 0')
            ->andWhere('goods.isRejected = 0')
        ;

        try{
            return $qb->getQuery()->getArrayResult();
        } catch(\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    /**
     * Находим товары, которые еще не модерировались
     * @return array|null
     */
    public function findAllWithRelated() {
        $qb = $this->createQueryBuilder('goods');
        $qb->select('goods, currency')
            ->leftJoin('goods.currency', 'currency')
            ->orderBy('goods.isModerated', 'DESC')
        ;

        try{
            return $qb->getQuery()->getArrayResult();
        } catch(\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
}