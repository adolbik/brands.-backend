<?php

namespace Brands\GoodsApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="goods")
 * @ORM\Entity(repositoryClass="Brands\GoodsApiBundle\Repository\GoodsRepository")
 * @ORM\HasLifecycleCallbacks()
*/

class Goods
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", nullable=false)
	 */
	protected $name;

	/**
	 * @var text
	 *
	 * @ORM\Column(name="description", type="text", nullable=true)
	 */
	protected $description;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="price", type="float", nullable=false)
	 */
	protected $price;

	/**
	 * @var $inStock
	 *
	 * @ORM\Column(name="in_stock", type="boolean", nullable=true, options={"default": 0})
	 */
	protected $inStock;

    /**
	 * @var $isModerated
	 *
	 * @ORM\Column(name="is_moderated", type="boolean", nullable=true, options={"default": 0})
	 */
	protected $isModerated = 0;

    /**
	 * @var $isRejected
	 *
	 * @ORM\Column(name="is_rejected", type="boolean", nullable=true, options={"default": 0})
	 */
	protected $isRejected = 0;

    /**
	 * @var $rejectComment
	 *
	 * @ORM\Column(name="reject_comment", type="text", nullable=true)
	 */
	protected $rejectComment;

	/**
	 * @var $isForMale
	 *
	 * @ORM\Column(name="is_for_male", type="boolean", nullable=true, options={"default": 0})
	 */
	protected $isForMale;

	/**
	 * @var $isForFemale
	 *
	 * @ORM\Column(name="is_for_female", type="boolean", nullable=true, options={"default": 0})
	 */
	protected $isForFemale;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="external_id", type="integer", nullable=true)
	 */
	protected $externalId;

    /**
	 * @var string
	 *
	 * @ORM\Column(name="external_url", type="string", nullable=true)
	 */
	protected $externalUrl;

    /**
	 * @var integer
	 *
	 * @ORM\Column(name="views", type="integer", nullable=true, options={"default": 0})
	 */
	protected $views = 0;

	/**
	 * @var \DateTime $created_at
	 *
	 * @ORM\Column(name="created_at", type="datetime", nullable=FALSE)
	 */
	private $createdAt;

	/**
	 * @var \DateTime $updated_at
	 *
	 * @ORM\Column(name="updated_at", type="datetime", nullable=true)
	 */
	private $updatedAt;


	/**
	 * @ORM\ManyToOne(targetEntity="\Brands\CoreBundle\Entity\CatalogItem")
	 * @ORM\JoinColumn(name="currency_id", referencedColumnName="id")
	 */
	protected $currency;

	/**
	 * @ORM\ManyToOne(targetEntity="\Brands\AdminBundle\Entity\Category")
	 * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
	 */
	protected $category;

	/**
	 * @ORM\ManyToOne(targetEntity="\Brands\UserBundle\Entity\User")
	 * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
	 */
	protected $owner;

	/**
	 * @var \Doctrine\Common\Collections\Collection
	 *
	 * @ORM\ManyToMany(targetEntity="\Brands\CoreBundle\Entity\CatalogItem")
	 * @ORM\JoinTable(name="goods_to_size",
	 *   joinColumns={
	 *     @ORM\JoinColumn(name="goods_id", referencedColumnName="id", onDelete="CASCADE")
	 *   },
	 *   inverseJoinColumns={
	 *     @ORM\JoinColumn(name="size_id", referencedColumnName="id")
	 *   }
	 * )
	 */
	protected $sizes;

	/**
	 * @ORM\OneToMany(targetEntity="\Brands\GoodsApiBundle\Entity\GoodsPhoto", mappedBy="goods", orphanRemoval=true, cascade={"persist"})
	 */
	protected $photos;

	protected $images;


	public function __construct() {
		$this->sizes = new \Doctrine\Common\Collections\ArrayCollection();
		$this->photos = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * @ORM\PrePersist
	 */
	public function prePersist()
	{
		$this->setCreatedAt(new \DateTime());
		$this->setUpdatedAt(new \DateTime());
	}

	/**
	 * @ORM\PreUpdate
	 */
	public function preUpdate()
	{
		$this->setUpdatedAt(new \DateTime());
	}

	public function isNew()
	{
		return $this->id ? false : true;
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Goods
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Goods
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Goods
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Goods
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Goods
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set currency
     *
     * @param \Brands\CoreBundle\Entity\CatalogItem $currency
     * @return Goods
     */
    public function setCurrency(\Brands\CoreBundle\Entity\CatalogItem $currency = null)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return \Brands\CoreBundle\Entity\CatalogItem 
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Add sizes
     *
     * @param \Brands\CoreBundle\Entity\CatalogItem $sizes
     * @return Goods
     */
    public function addSize(\Brands\CoreBundle\Entity\CatalogItem $sizes)
    {
        $this->sizes[] = $sizes;

        return $this;
    }

    /**
     * Remove sizes
     *
     * @param \Brands\CoreBundle\Entity\CatalogItem $sizes
     */
    public function removeSize(\Brands\CoreBundle\Entity\CatalogItem $sizes)
    {
        $this->sizes->removeElement($sizes);
    }

    /**
     * Get sizes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSizes()
    {
        return $this->sizes;
    }

    /**
     * Add photos
     *
     * @param \Brands\GoodsApiBundle\Entity\GoodsPhoto $photos
     * @return Goods
     */
    public function addPhoto(\Brands\GoodsApiBundle\Entity\GoodsPhoto $photos)
    {
        $this->photos[] = $photos;

        return $this;
    }

	/**
	 * Set photos
	 *
	 * @return Goods
	 */
	public function setPhotos($photos)
	{
		$this->photos = $photos;

		return $this;
	}

    /**
     * Remove photos
     *
     * @param \Brands\GoodsApiBundle\Entity\GoodsPhoto $photos
     */
    public function removePhoto(\Brands\GoodsApiBundle\Entity\GoodsPhoto $photos)
    {
        $this->photos->removeElement($photos);
    }

    /**
     * Get photos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * Set category
     *
     * @param \Brands\AdminBundle\Entity\Category $category
     * @return Goods
     */
    public function setCategory(\Brands\AdminBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Brands\AdminBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

	/**
	 * Set inStock
	 *
	 * @param integer $inStock
	 * @return Goods
	 */
	public function setInStock($inStock)
	{
		$this->inStock = $inStock;

		return $this;
	}

	/**
	 * Get inStock
	 *
     * @return boolean
	 */
	public function getInStock()
	{
		return $this->inStock;
	}

    /**
     * Set isModerated
     *
     * @param integer $isModerated
     * @return Goods
     */
    public function setIsModerated($isModerated)
    {
        $this->isModerated = $isModerated;

        return $this;
    }

    /**
     * Get isModerated
     *
     * @return boolean
     */
    public function getIsModerated()
    {
        return $this->isModerated;
    }

    /**
     * Set isRejected
     *
     * @param integer $isRejected
     * @return Goods
     */
    public function setIsRejected($isRejected)
    {
        $this->isRejected = $isRejected;

        return $this;
    }

    /**
     * Get isRejected
     *
     * @return boolean
     */
    public function getIsRejected()
    {
        return $this->isRejected;
    }

    /**
     * Set rejectComment
     *
     * @param string $rejectComment
     * @return Goods
     */
    public function setRejectComment($rejectComment)
    {
        $this->rejectComment = $rejectComment;

        return $this;
    }

    /**
     * Get rejectComment
     *
     * @return string 
     */
    public function getRejectComment()
    {
        return $this->rejectComment;
    }

	/**
	 * @param mixed $images
	 */
	public function setImages($images)
	{
		$this->images = $images;
	}

	/**
	 * @return mixed
	 */
	public function getImages()
	{
		return $this->images;
	}

    /**
     * Set owner
     *
     * @param \Brands\UserBundle\Entity\User $owner
     * @return Goods
     */
    public function setOwner(\Brands\UserBundle\Entity\User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \Brands\UserBundle\Entity\User 
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set isForMale
     *
     * @param integer $isForMale
     * @return Goods
     */
    public function setIsForMale($isForMale)
    {
        $this->isForMale = $isForMale;

        return $this;
    }

    /**
     * Get isForMale
     *
     * @return boolean 
     */
    public function getIsForMale()
    {
        return $this->isForMale;
    }

    /**
     * Set isForFemale
     *
     * @param integer $isForFemale
     * @return Goods
     */
    public function setIsForFemale($isForFemale)
    {
        $this->isForFemale = $isForFemale;

        return $this;
    }

    /**
     * Get isForFemale
     *
     * @return boolean 
     */
    public function getIsForFemale()
    {
        return $this->isForFemale;
    }

	/**
	 * @return int
	 */
	public function getExternalId()
	{
		return $this->externalId;
	}

	/**
	 * @param int $externalId
	 */
	public function setExternalId($externalId)
	{
		$this->externalId = $externalId;
	}

	/**
	 * @return string
	 */
	public function getExternalUrl()
	{
		return $this->externalUrl;
	}

	/**
	 * @param string $externalUrl
	 */
	public function setExternalUrl($externalUrl)
	{
		$this->externalUrl = $externalUrl;
	}

	/**
	 * @return int
	 */
	public function getViews()
	{
		return $this->views;
	}

	/**
	 * @param int $views
	 */
	public function setViews($views)
	{
		$this->views = $views;
	}
}
