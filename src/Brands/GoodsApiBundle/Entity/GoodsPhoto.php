<?php

namespace Brands\GoodsApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="goods_photo")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
*/

class GoodsPhoto
{
	/**
	 * @var integer $id
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string $src
	 *
	 * @ORM\Column(name="src", type="string", nullable=false, options={"comment" = "Название файла"})
	 *
	 */
	private $src;

	/**
	 * @var boolean $isMain
	 *
	 * @ORM\Column(name="is_main", type="string", nullable=false, options={"comment" = "Является ли картинка главной", "default": 0})
	 *
	 */
	private $isMain = 0;

	/**
	 * @var array $extra
	 *
	 * @ORM\Column(name="extra", type="array", nullable=true)
	 *
	 */
	private $extra;

	/**
	 * @var \DateTime $created_at
	 *
	 * @ORM\Column(name="created_at", type="datetime", nullable=FALSE)
	 */
	private $createdAt;

	/**
	 * @var \DateTime $updated_at
	 *
	 * @ORM\Column(name="updated_at", type="datetime", nullable=true)
	 */
	private $updatedAt;


	/**
	 * @ORM\ManyToOne(targetEntity="\Brands\GoodsApiBundle\Entity\Goods", inversedBy="photos")
	 * @ORM\JoinColumn(name="goods_id", referencedColumnName="id", onDelete="CASCADE")
	 */
	protected $goods;


	/**
	 * @ORM\PrePersist
	 */
	public function prePersist()
	{
		$this->setCreatedAt(new \DateTime());
		$this->setUpdatedAt(new \DateTime());
	}

	/**
	 * @ORM\PreUpdate
	 */
	public function preUpdate()
	{
		$this->setUpdatedAt(new \DateTime());
	}


	public function isNew()
	{
		return $this->id ? false : true;
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set src
     *
     * @param string $src
     * @return GoodsPhoto
     */
    public function setSrc($src)
    {
        $this->src = $src;

        return $this;
    }

    /**
     * Get src
     *
     * @return string 
     */
    public function getSrc()
    {
        return $this->src;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return GoodsPhoto
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return GoodsPhoto
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set goods
     *
     * @param \Brands\GoodsApiBundle\Entity\Goods $goods
     * @return GoodsPhoto
     */
    public function setGoods(\Brands\GoodsApiBundle\Entity\Goods $goods = null)
    {
        $this->goods = $goods;

        return $this;
    }

    /**
     * Get goods
     *
     * @return \Brands\GoodsApiBundle\Entity\Goods 
     */
    public function getGoods()
    {
        return $this->goods;
    }

    /**
     * Set isMain
     *
     * @param string $isMain
     * @return GoodsPhoto
     */
    public function setIsMain($isMain)
    {
        $this->isMain = $isMain;

        return $this;
    }

    /**
     * Get isMain
     *
     * @return string 
     */
    public function getIsMain()
    {
        return $this->isMain;
    }

    /**
     * Set extra
     *
     * @param string $extra
     * @return GoodsPhoto
     */
    public function setExtra($extra)
    {
        $this->extra = $extra;

        return $this;
    }

    /**
     * Get extra
     *
     * @return string 
     */
    public function getExtra()
    {
        return $this->extra;
    }
}
