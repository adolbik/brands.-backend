<?php

namespace Brands\GoodsApiBundle\Service;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Goods
{
	private $_container;
	private $_em;
	private $_fileUploadHandler;
	private $_router;
	private $_categoryService;

    /**
     * @param $em
     * @param $fileUploadHandler
     */
    public function __construct(ContainerInterface $container)
	{
		$this->_container = $container;
		$this->_em = $container->get('doctrine')->getManager();
		$this->_fileUploadHandler = $container->get('brands.file_upload_handler');
		$this->_router = $container->get('router');
		$this->_categoryService = $container->get('brands.category');
	}

	public function removeGoods($goods) {
        $photos = $goods->getPhotos();
        foreach($photos as $p) {
            $this->_fileUploadHandler->removeFile($p->getSrc(), 'goods_photo');
        }
        $this->_em->remove($goods);
        $this->_em->flush();

		$category = $goods->getCategory();
		$user = $goods->getOwner();

		$this->_categoryService->setCategoryGoodsCount($category);
		$this->_categoryService->setCategoryUserGoodsCount($category, $user);
		$this->_em->flush();

        return true;
    }

	/**
	 * Return entity with thumbnails path
	 * @var $entity
	 * @var string $fileContext FileLoader context
	 */
	public function addEntityPhotosPath($entity, $fileContext) {
		$request = $this->_container->get('request');
		$baseUrl = $request->getSchemeAndHttpHost();

		if(!is_array($entity)) {
			$entities[] = $entity;
		} else {
			$entities = $entity;
		}
		foreach($entities as $e) {
			if(!$e || !$e->getPhotos()) {
				continue;
			}
			$photos = $e->getPhotos();

			$fileLoader = $this->_container->get('brands.file_loader');
			$fileLoader->configure($fileContext);
			$imageFolder = $fileLoader->getFolder();

			foreach($photos as $photo) {
				$imageOriginal = ($photo && $photo->getSrc()) ?  $imageFolder.$photo->getSrc() : '';
				if($imageOriginal) {
					$cacheManager = $this->_container->get('liip_imagine.cache.manager');
					$srcPath_54x50 = $cacheManager->getBrowserPath($imageOriginal, '54x50');
					$originalName = $photo->getSrc();

					$arr = array('original_name' => $originalName, '54x50' => $srcPath_54x50, 'path' => $baseUrl.$imageOriginal);
				} else {
					$arr = array();
				}

				$photo->setExtra($arr);
			}
		}

		return $entity;
	}

}