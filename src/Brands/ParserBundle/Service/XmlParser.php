<?php

namespace Brands\ParserBundle\Service;

use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\ORM\EntityManager;
use Brands\GoodsApiBundle\Entity\Goods;

class XmlParser
{
	protected $em;
	protected $categories;
	protected $categoriesTree;
	protected $response;
	protected $xml;

    /**
     * @param $em
     */
    public function __construct(EntityManager $em)
	{
		$this->em = $em;
	}

	/**
	 * Парсим xml
	 * @param $url
	 */
	public function parseXmlAction($url)
	{
		$xml = simplexml_load_file($url);
		$this->xml = $xml;
		$xmlArray = json_decode(json_encode((array)$xml),1);

		$offers = $xmlArray['shop']['offers']['offer'];
		foreach($offers as $offer) {
			$this->saveOffer($offer);
		}
	}

	/**
	 * Сохраняем / обновляем заказы из xml
	 * @param $offer
	 */
	protected function saveOffer($offer)
	{
		$externalId = $offer['@attributes']['id'];
		$projectCategory = $this->checkOfferCategory($offer['model'], $offer['categoryId']);

		if($projectCategory) {
			$goods = $this->em->getRepository('BrandsGoodsApiBundle:Goods')->findOneBy(array('externalId' => $externalId));
			if(!$goods) {
				$goods = new Goods();
			}
			$goods->setExternalId($externalId);
			$goods->setPrice($offer['price']);
			$goods->setName($offer['model']);
			$goods->setDescription($offer['description']);
			$goods->setCategory($projectCategory);
		} else {
			$this->response['errors'][$externalId] = 'Товар с id ' . $externalId . ' не добавлен. Причина: не удалось найти подходящую категорию';
		}

		die();
	}

	/**
	 * Найти подходящую категорию в проекте для товара
	 * @param      $offer
	 * @param null $offerCategoryId
	 *
	 * @return bool|mixed|null
	 */
	public function checkOfferCategory($offerName, $offerCategoryId) {
		$categories = $this->getCategories();
		$offerCategory = $categories[$offerCategoryId];
		$offerCategoryName = $offerCategory['name'];

		$categoryNameArr = explode(' ', $offerCategoryName);
		foreach($categoryNameArr as $cn) {
			$projectCategory = $this->em->getRepository('BrandsAdminBundle:Category')->findCategoryBySynonyms($cn);
			if($projectCategory) {
				break;
			}
		}

		if($projectCategory) { // если нашли по соответствие по радительсткой категории
			return $projectCategory;
		} else { // ищем соответствие по родительским категориям
			$parentCategory = $this->getParentCategory($offerCategoryId);
			if($parentCategory) {
				return $this->checkOfferCategory($offerName, $parentCategory);
			} else { // если дальше нет родительских категорий, то ищем по названию
				$this->checkCategoryByOfferName($offerName);
			}
		}

		return false;
	}

	protected function checkCategoryByOfferName($offerName) {
		$nameArr = explode(' ', $offerName);
		foreach($nameArr as $n) {
			$projectCategory = $this->em->getRepository('BrandsAdminBundle:Category')->findCategoryBySynonyms($n);
			if($projectCategory) {
				return $projectCategory;
			}
		}

		return false;
	}

	/**
	 * Массив категорий
	 * @param null $xml
	 *
	 * @return array
	 */
	protected function getCategories($xml = null) {
		$xml = $xml ? $xml : $this->xml;
		if($this->categories) {
			return $this->categories;
		}

		$categories = $xml->shop->categories->category;

		$categoriesArr = array();
		foreach($categories as $c) {
			$attributes = $c->attributes();
			$categoryId = (string)$attributes['id'];
			$categoryParentId = (string)$attributes['parentId'];
			$categoriesArr[$categoryId] = array('name' => (string)$c, 'parentId' => $categoryParentId);
		}

		return $categoriesArr;
	}

	/**
	 * Дерево категорий
	 * @param null $xml
	 *
	 * @return array
	 */
	protected function getCategoriesTree($xml = null) {
		$xml = $xml ? $xml : $this->xml;
		if($this->categoriesTree) {
			return $this->categoriesTree;
		}

		$categories = $xml->shop->categories->category;

		$categoriesArr = array();
		foreach($categories as $c) {
			$attributes = $c->attributes();
			$categoryId = (string)$attributes['id'];
			$categoryParentId = (string)$attributes['parentId'];
			if($categoryParentId) {
				$categoriesArr[$categoryParentId][$categoryId] = array('name' => (string)$c, 'children' => array());
			} else {
				$categoriesArr[$categoryId] = array('name' => (string)$c, 'children' => array());
			}
		}

		return $categoriesArr;
	}

	/**
	 * Получить id родительской категории
	 * @param $categoryId
	 *
	 * @return array|null
	 */
	protected function getParentCategory($categoryId) {
		$categoriesList = $this->getCategories();
		$parentCategoryId = $categoriesList[$categoryId]['parentId'];

		return isset($categoriesList[$parentCategoryId]) ? $parentCategoryId : null;
	}

	/**
	 * Уставнавливаем кастомные категории
	 * @param $customCategories
	 */
	public function setCustomCategories($customCategories) {
		$this->categories = $customCategories;
	}
}