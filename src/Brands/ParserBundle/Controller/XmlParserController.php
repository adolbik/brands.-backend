<?php

namespace Brands\ParserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class XmlParserController extends Controller
{

    public function parseXmlAction(Request $request)
    {
        $pathToXmlFile = 'http://192.168.10.118:10/example.xml';

		$parser = $this->get('brands.parser.xml');
		$customCategories = array(
			'1' => array('name' => 'Одежда', 'parentId' => ''),
			'2' => array('name' => 'Обувь', 'parentId' => ''),
			'3' => array('name' => 'Рубашки', 'parentId' => '1'),
			'4' => array('name' => 'Сорочки', 'parentId' => '3'),
			'5' => array('name' => 'Штаны', 'parentId' => '1'),
			'6' => array('name' => 'Джинсы', 'parentId' => '5')
		);

		$parser->setCustomCategories($customCategories);

		$a = $parser->checkOfferCategory('Сорочка цветная', '4');
		exit(\Doctrine\Common\Util\Debug::dump($a));



		$this->get('brands.parser.xml')->parseXmlAction($pathToXmlFile);
		die();
	}

	public function sendCategoryForParsedOfferAction(Request $request)
    {
		$categoryDelimiter = ',';
		$offerName = $request->get('name');
		$categories = $request->get('categories');
		$categoriesArr = explode($categoryDelimiter, $categories);

		$customCategories = array();
		foreach($categoriesArr as $k => $c) {
			$customCategories[$k] = array('name' => $c, 'parentId' => $k ? ($k-1) : '');

			/*$customCategories = array(
				'1' => array('name' => 'Одежда', 'parentId' => ''),
				'2' => array('name' => 'Обувь', 'parentId' => ''),
				'3' => array('name' => 'Рубашки', 'parentId' => '1'),
				'4' => array('name' => 'Сорочки', 'parentId' => '3'),
				'5' => array('name' => 'Штаны', 'parentId' => '1'),
				'6' => array('name' => 'Джинсы', 'parentId' => '5')
			);*/
		}

        $pathToXmlFile = 'http://192.168.10.118:10/example.xml';

		$parser = $this->get('brands.parser.xml');
		$customCategories = array(
			'1' => array('name' => 'Одежда', 'parentId' => ''),
			'2' => array('name' => 'Обувь', 'parentId' => ''),
			'3' => array('name' => 'Рубашки', 'parentId' => '1'),
			'4' => array('name' => 'Сорочки', 'parentId' => '3'),
			'5' => array('name' => 'Штаны', 'parentId' => '1'),
			'6' => array('name' => 'Джинсы', 'parentId' => '5')
		);

		$parser->setCustomCategories($customCategories);

		$a = $parser->checkOfferCategory('Сорочка цветная', '4');
		exit(\Doctrine\Common\Util\Debug::dump($a));



		$this->get('brands.parser.xml')->parseXmlAction($pathToXmlFile);
		die();
	}
}
