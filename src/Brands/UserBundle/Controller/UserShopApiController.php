<?php

namespace Brands\UserBundle\Controller;

use Brands\ShopBundle\Entity\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Brands\UserBundle\Entity\User;
use Brands\UserBundle\Form\UserWithShopType;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\Form\FormError;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class UserShopApiController extends Controller
{
	/**
	 * Edit user with shop
	 *
	 * @ApiDoc(
	 *  resource=true,
	 *  description="Edit user with shop",
	 *  input="Brands\UserBundle\Form\UserWithShopType"
	 * )
	 *
	 * @Rest\View
	 */
	public function editAction()	{
		$em = $this->getDoctrine()->getManager();
		$user = $this->getUser();
		$user = $em->getRepository('BrandsUserBundle:User')->findOneWithShop($user->getId());

		if(!$user) {
			$responseData = array('error' => 'User is not authenticated');
			return $this->get('brands.api.response')->generateResponse($responseData, Response::HTTP_NOT_FOUND);
		}

		return $this->processForm($user);
	}

	/**
	 * Get a goods info
	 *
	 * @ApiDoc(
	 *  resource=true,
	 *  description="Get a goods info",
	 * 	requirements={
	 *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Goods id"}
	 *  }
	 * )
	 *
	 * @Rest\View
	 */
	public function getAction()	{
		$em = $this->getDoctrine()->getManager();
		$user = $this->getUser();
		$user = $em->getRepository('BrandsUserBundle:User')->findOneWithShop($user->getId());

		if(!$user) {
			$responseData = array('error' => 'User is not authenticated');
			return $this->get('brands.api.response')->generateResponse($responseData, Response::HTTP_NOT_FOUND);
		}

		$this->get('brands.shop')->addEntityLogoPath($user->getShops(), 'shop_logo');


		/*$form = $this->createForm(
			new UserWithShopType($this->get('brands.file_upload_handler')),
			$user
		);

		return $this->render('BrandsUserBundle:UserShopApi:get.html.twig', array('form' => $form->createView()));*/

		return $this->get('brands.api.response')->generateResponse(array('user' => $user), Response::HTTP_OK);
	}

	/**
	 * Save / Edit user with shop
	 *
	 * @Rest\View
	 */
	public function processForm(User $user) {
		$request = $this->get('request');
		$statusCode = $user->getId() ? Response::HTTP_CREATED : Response::HTTP_OK;

		$form = $this->createForm(new UserWithShopType($this->get('brands.file_upload_handler')), $user);
		$form->handleRequest($request);

		if ($form->isValid()) {
			$em = $this->getDoctrine()->getManager();

			if($isNew = $user->getId()) {
				$em->persist($user);
			}

			$userManager = $this->container->get('fos_user.user_manager');
			$userManager->updateUser($user);

			$em->flush();

			$responseData = array();
			if (Response::HTTP_CREATED === $statusCode) {
				$responseData['id'] = $user->getId();
			}

			return $this->get('brands.api.response')->generateResponse($responseData, $statusCode);
		}

		return $this->get('brands.api.response')->generateResponse(array('form' => $form), Response::HTTP_BAD_REQUEST);
	}
}
