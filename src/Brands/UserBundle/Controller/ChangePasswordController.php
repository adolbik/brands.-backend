<?php

namespace Brands\UserBundle\Controller;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Brands\UserBundle\Form\UserChangePasswordType;
use Symfony\Component\HttpFoundation\Response;

class ChangePasswordController extends Controller
{
    /**
     * Change user password
     */
    public function changePasswordAction(Request $request)
	{
		$user = $this->container->get('security.context')->getToken()->getUser();
		if (!is_object($user) || !$user instanceof UserInterface) {
			//return $this->get('brands.api.response')->generateResponse(array('form' => $form), Response::HTTP_BAD_REQUEST);
			throw new AccessDeniedException('This user does not have access to this section.');
		}

		/** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
		$dispatcher = $this->container->get('event_dispatcher');

		$form = $this->createForm(new UserChangePasswordType(), $user);
		$form->setData($user);

		if ($request->isMethod('POST')) {
			$form->handleRequest($request);

			if ($form->isValid()) {
				/** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
				$userManager = $this->container->get('fos_user.user_manager');

				$event = new FormEvent($form, $request);
				$dispatcher->dispatch(FOSUserEvents::CHANGE_PASSWORD_SUCCESS, $event);

				$userManager->updateUser($user);
				// $dispatcher->dispatch(FOSUserEvents::CHANGE_PASSWORD_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

				$responseData = array();
				return $this->get('brands.api.response')->generateResponse($responseData, Response::HTTP_OK);
			}
		}

		return $this->get('brands.api.response')->generateResponse(array('form' => $form), Response::HTTP_BAD_REQUEST);
	}
}
