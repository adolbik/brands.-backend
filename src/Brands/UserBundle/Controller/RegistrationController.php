<?php

namespace Brands\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use FOS\UserBundle\Controller\RegistrationController as BaseController;

class RegistrationController extends BaseController
{
    /**
     * Tell the user his account is now confirmed
     */
    public function confirmedAction()
    {
		return $this->container->get('templating')->renderResponse('BrandsUserBundle:Registration:register_completed.html.twig');
	}
}
