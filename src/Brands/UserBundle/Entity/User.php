<?php

namespace Brands\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="Brands\UserBundle\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks()
*/

class User extends BaseUser
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	protected $id;

	/**
	 * @ORM\OneToMany(targetEntity="\Brands\ShopBundle\Entity\Shop", mappedBy="owner", orphanRemoval=true, cascade={"persist"})
	 */
	protected $shops;

	public function __construct() {
        parent::__construct();

		$this->shops = new ArrayCollection();
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add shops
     *
     * @param \Brands\ShopBundle\Entity\Shop $shops
     * @return User
     */
    public function addShop(\Brands\ShopBundle\Entity\Shop $shops)
    {
		$shops->setOwner($this);
        $this->shops[] = $shops;

        return $this;
    }

    /**
     * Remove shops
     *
     * @param \Brands\ShopBundle\Entity\Shop $shops
     */
    public function removeShop(\Brands\ShopBundle\Entity\Shop $shops)
    {
        $this->shops->removeElement($shops);
    }

    /**
     * Get shops
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getShops()
    {
        return $this->shops;
    }
}
