<?php

namespace Brands\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class BrandsUserBundle extends Bundle
{
	public function getParent()
	{
		return 'FOSUserBundle';
	}
}
