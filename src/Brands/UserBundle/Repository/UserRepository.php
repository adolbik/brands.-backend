<?php
namespace Brands\UserBundle\Repository;

use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{

	public function findOneWithShop($userId) {
		$qb = $this->createQueryBuilder('user');
		$qb->select('user, shops')
			->leftJoin('user.shops', 'shops')
			->where('user.id = :userId')
		;

		$qb->setParameter('userId', $userId);

		try{
			return $qb->getQuery()->getOneOrNullResult();
		} catch(\Doctrine\ORM\NoResultException $e) {
			return null;
		}
	}
}