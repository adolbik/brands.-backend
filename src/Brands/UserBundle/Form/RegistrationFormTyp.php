<?php

namespace Brands\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Brands\ShopBundle\Form\RegisterShopType;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Brands\ShopBundle\Entity\Shop;

class RegistrationFormTyp extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', 'email', array('label' => 'form.email', 'translation_domain' => 'FOSUserBundle'))
            ->add('username', null, array('label' => 'form.username', 'translation_domain' => 'FOSUserBundle'))
            ->add('plainPassword', 'repeated', array(
                'type' => 'password',
                'options' => array('translation_domain' => 'FOSUserBundle'),
                'first_options' => array('label' => 'form.password'),
                'second_options' => array('label' => 'form.password_confirmation'),
                'invalid_message' => 'fos_user.password.mismatch',
            ))

            ->add('shops', 'collection', array(
                'type' => new RegisterShopType(),
                'allow_add'    => true,
                'allow_delete' => true,
                'by_reference' => true,
				'cascade_validation' => true
            ))
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA , array($this, 'preSetData'));
    }

    public function preSetData(FormEvent $event) {
        $user = $event->getData();
        $form = $event->getForm();

        if($user && !$user->getShops()->count()) {
            $shop = new Shop();
            $user->addShop($shop);
        }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Brands\UserBundle\Entity\User',
            'intention'  => 'registration',
			'cascade_validation' => true
        ));
    }

    public function getName()
    {
        return 'brands_user_registration';
    }
}
