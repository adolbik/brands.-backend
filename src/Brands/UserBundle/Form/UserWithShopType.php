<?php

namespace Brands\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Brands\ShopBundle\Form\ShopType;
use Brands\CoreBundle\Handler\FileUploadHandler;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;

class UserWithShopType extends AbstractType
{
	protected $fileUploadHandler;

	public function __construct(FileUploadHandler $fileUploadHandler) {
		$this->fileUploadHandler = $fileUploadHandler;
	}

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('username')
            ->add('email')

			->add('shops', 'collection', array(
				 'type' => new ShopType($this->fileUploadHandler),
				 'allow_add'    => true,
				 'allow_delete' => true,
				 'by_reference' => false,
				 'cascade_validation' => true
			))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Brands\UserBundle\Entity\User',
			'csrf_protection'   => false
        ));
    }

    public function getName()
    {
        return 'user';
    }
}
