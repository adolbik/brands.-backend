<?php

namespace Brands\UserBundle\Form;

use FOS\UserBundle\Form\Type\ChangePasswordFormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class UserChangePasswordType extends ChangePasswordFormType
{

	public function __construct() {}

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		parent::buildForm($builder, $options);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Brands\UserBundle\Entity\User',
			'intention'  => 'change_password',
			'csrf_protection'   => false
        ));
    }

    public function getName()
    {
        return 'user_password';
    }
}
