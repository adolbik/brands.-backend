<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new Brands\CoreBundle\BrandsCoreBundle(),
            new Brands\AdminBundle\BrandsAdminBundle(),
			new Bc\Bundle\BootstrapBundle\BcBootstrapBundle(),
			new Xsolve\BootstrapCrudBundle\XsolveBootstrapCrudBundle(),
			new Knp\Bundle\MenuBundle\KnpMenuBundle(),
			new Liip\ImagineBundle\LiipImagineBundle(),
			new FOS\RestBundle\FOSRestBundle(),
			new JMS\SerializerBundle\JMSSerializerBundle(),
			new Nelmio\ApiDocBundle\NelmioApiDocBundle(),
            new Brands\GoodsApiBundle\BrandsGoodsApiBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new Brands\UserBundle\BrandsUserBundle(),
			new FOS\OAuthServerBundle\FOSOAuthServerBundle(),
            new Brands\OAuthBundle\BrandsOAuthBundle(),
            new Brands\ShopBundle\BrandsShopBundle(),
            new Brands\ParserBundle\BrandsParserBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
